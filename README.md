

# MODEL ZOO 使用说明

## 一、简介

本工程主要包含3部分：`nnio`、`modelzoo_sim`、`modelzoo_socket`

1. `nnio`: 主要包括3部分：前处理、后处理、网络信息获取；输入输出数据处理
2. `modelzoo_socket`为socekt链接模式的上板前向运行时工程；（建议使用模型库中的运行时工程）
3. `modelzoo_sim`为simulate仿真前向；🚩目前仅支持不带硬算子仿真，请编译时去掉`[config]`部分（不影响精度和最终结果）；

## 二、环境准备

使用前请注意以下几点：

1. 由于opencv lib较大，请单独下载，并放置于 thirdparty\lib文件夹中

   [点击下载](https://download.fdwxhb.com/data/04_FMSH-100AI/100AI/04_modelzoo/modelzoo_pub/pkgs/opencv_lib_for_sim_220/opencv_world.lib)

   需要下载站权限

2. 打开sln**确保为release模式**

   ![image-20220808133905146](assets/image-20220808133905146-16599375310272.png)

3. **确认语言标准为c++ 17**

   ![image-20220808134120375](assets/image-20220808134120375.png)

4. **配置属性-调试修改：**

   - 设置配置文件路径：`socket`提供了`demo.yaml`例子，`simulate`提供了`sim.yaml`例子；

   - 将thirdparty的dll加入环境中，dll主要用了yaml，simulate等

     <img src="assets/image-20230403191009073.png" alt="	" style="zoom: 80%;" />

   - **socket:**

   ​		环境：	`PATH=%PATH%;../../thirdparty/dll;`   			

   - **sim:**

   ​		环境： `PATH=%PATH%;../../thirdparty/dll;C:\Icraft-CLI\Rely;`

5. 编译过程提示找不到ir virsion相关头文件，可以将那行include注释掉
6. 设置modelzoo_sim为启动项

## 三、使用方法

该程序通过sim.yaml配置要跑的模型，以及其必要的配置参数

**yaml文件配置说明：**

<div style="background-color: #FFFFCC; color: #000000; padding: 10px; border-left: 5px solid #FFA500;">
注意：</br>
1.yaml的冒号后面要留1个空格，不能多也不能少。以及不可随意修改缩进，容易影响层级关系，建议在 sim.yaml例子基础上修改，不要随意增改格式</br>
2. 仿真工程不能运行带硬算子的网络，因此编译用于仿真的 ini时，imagemake和 icorepost需要去掉，如图（注释需要用井号“#”，不能用分号“；”）</br>
</div>


下面就以 `yolov5s`为例，在注释中详细介绍我们需要修改的各项配置

1. 配置要跑哪个模型

   ```yaml
   # 整体models部分，决定这次运行要跑哪些网络，分别使用什么前后处理
   models: [
              [yolov5_sim,yolov5s_8,yolov5],
              # 对应关系：[前处理、模型前向json&raw及结果保存路径、后处理参数及测试集配置]
              #[yolov3_sim,yolov3_8,yolov3],
              # 此次不运行
              ]
   ```

2. 配置路径相关

   ```yaml
   # 具体网络文件配置，此处以量化阶段举例，其实包括parse、optimize、quantize、adapt、generate的所有阶段都可以仿真
   networks:
     yolov5s_8:
       jsonPath: E:\YoloV5s_8\YoloV5s_8_quantized.json
       # 编译生成的json文件
       rawPath: E:\YoloV5s_8\YoloV5s_8_quantized.raw
       # 编译生成的raw文件
       resRoot: *tempres
       # 结果图片存放路径，这里用了全局参数存放在临时文件夹，跑单张图时可以使用
       resRoot: E:\rsl\yolov5s_8_month_day\
       # 精度测试时，推荐配置为本地单独的文件夹，路径的父文件夹必须存在，路径末尾必须有“\”
       show: *show
       # 全局参数：是否显示结果图片
   ```

   

3. 配置前后处理参数

   ```yaml
   # 后处理参数及测试集配置，若使用coco数据集的官方模型，一般不需要修改
   params:
    yolov5:
     conf: *conf
     # 全局参数：置信度
     iou: 0.65
     # 重叠度，与浮点精度测试时使用的数值对齐即可
     number_of_class: 80
     # 类别数
     number_of_head: 3
     # 检测头数量
     anchors: [[[10.,13.],[16.,30.],[33.,23.]],[[30.,61.],[62.,45.],[59.,119.]],[[116.,90.],[156.,198.],[373.,326.]]]	
     # 锚框
     multilable: *multilable
     # 全局参数：多标签检测，精度测试时打开
     imgRoot: *lc_cocoval2017
     # 全局参数：coco测试集文件夹
     imgList: *lc_cocoval2017_list
     # 全局参数：由coco测试集文件夹中图片文件名列表保存成的txt
     names: *coconames
     # 全局参数：数据集类别名称文件，show=true时可以显示类别名称
     
   # 需要修改的例子
     ddrnet:
       imgRoot: E:\mIoU_test\CITYSCAPES\
       # 测试集文件夹
       imgList: E:\mIoU_test\CITYSCAPES\valImages.txt
       # 由测试集文件夹中图片文件名列表保存成的txt
     resnet:
       imgRoot: E:\ImageNet2012_Test5000\images/
       # 测试集文件夹
       imgList: E:\ImageNet2012_Test5000\Test5000_imgList.txt
       # 由测试集文件夹中图片文件名列表保存成的txt
       names: E:\Gluon_ResNet\names\imagenet.names
       # 数据集类别名称文件，show=true时可以显示类别名称
       resize_hw: [256,256]
       # 前处理，与模型的浮点工程对齐即可，先resize，大部分分类网络不需要修改
       crop_hw: [224,224]
       # 前处理，resize之后中心裁剪，中心裁剪后即为网络input的大小，大部分分类网络不需要修改
   ```

4. 关于全局配置的说明

   ```yaml
   # 全局参数配置，跑多个网络只需要修改此一处
   conf: &conf 0.3
   # 运行单例观察效果时，可如示例中，设置较高置信度作为阈值，精度测试时须设为0.001
   show: &show true
   # 是否显示结果图片，精度测试时关闭
   lc_cocoval2017: &lc_cocoval2017 D:\datasets\val2017\
   # 若待测网络使用coco数据集，可在此处统一配置测试集文件夹，否则不用配置
   lc_cocoval2017_list: &lc_cocoval2017_list D:\datasets\cocoval_2017.txt
   # 同上，txt文件为上面文件夹中的图片文件名列表
   coconames: &coconames D:\Icraft\workspace_v2.0\icraft-tutorial\networks\names\coco.names
   # 数据集类别名称文件
   multilable: &multilable false
   # 是否使用多标签检测，打开后一个目标可以记录多个类别，精度测试时须打开
   tempres: &tempres D:\Icraft\restemp\temp
   # 设置临时结果图片存放路径，精度测试时不推荐使用
   ```

   

## 

声明：本工程仅作为研究、参考；受开发条件限制，有些模型前后处理未做严谨的测试验证，请勿未经验证直接用于项目开发。