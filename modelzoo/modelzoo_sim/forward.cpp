#include "runtime.h"
#include "icraft-ir/hard_op.h"
#include "fmt/core.h"
#include "yaml-cpp/yaml.h"
#include "PicPre.hpp"
#include "utils.hpp"
#include "NetInfo.hpp"
#include "forward.h"
#include "factory.hpp"
#include "NetInfo_sim.hpp"
#include "YoloSoft.hpp"
#include "YoloV5_sim.hpp"
#include "YoloV3_sim.hpp"
#include "YoloV6_sim.hpp"
#include "YoloV8_sim.hpp"
#include "YoloX_sim.hpp"
#include "PPYoloE_sim.hpp"
#include "YoloV5_pose_sim.hpp"
#include "YoloV5_obb_sim.hpp"
#include "YoloV5_seg_sim.hpp"
#include "seg.hpp"
#include "YoloV8_pose_sim.hpp"
#include "BYTETracker.h"

void yolo(const YAML::Node& network, const YAML::Node& param, std::string classname, const int inputFlag) {

    //-------------icraft config-----------------------//
    std::string jsonPath = network["jsonPath"].as<std::string>();
    std::string rawPath = network["rawPath"].as<std::string>();
    std::string resRoot = network["resRoot"].as<std::string>();
    bool show = network["show"].as<bool>();
    checkDir(resRoot);
    //--------------network config-----------------------//
    Yolo::extCfg cfg;
    // post relate
    cfg.CONF = param["conf"].as<float>();
    cfg.IOU = param["iou"].as<float>();
    cfg.NOC = param["number_of_class"].as<int>();
    cfg.NOH = param["number_of_head"].as<int>();
    cfg.ANCHORS = param["anchors"].as<std::vector<std::vector<std::vector<float>>>>();
    cfg.MULTILABLE = param["multilable"].as<bool>();

    // dataset relate
    std::string imgRoot = param["imgRoot"].as<std::string>();
    std::string imgList = param["imgList"].as<std::string>();
    std::string names_path = param["names"].as<std::string>();
    auto names = toVector(names_path);

    //-------------SET SIMULATOR-----------------------//
    std::map < std::string, std::string > options;
    options["json"] = jsonPath;
    options["raw"] = rawPath;
    options["log_path"] = ".log/";

    //generate can not use cudamode
    //目前打开cudamode连续推理会出bug，等待修复
    options["cudamode"] = "true";
    options["fake_qf"] = "false";
    // options["dump_ftmp"] = "SFB";
 
    //default
    options["target"] = "BUYI";
    options["show"] = "false";
    options["save"] = "false";
    
    auto network_ptr = std::make_shared<icraft::sim::SimulatorNetwork>(options);
    NetInfo_sim netinfo(network_ptr);

    //------------- PICS -----------------------//
    int index = 0;
    auto namevector = toVector(imgList);
    int totalnum = namevector.size();
    for (auto name : namevector) {
        progress(index, totalnum);
        index++;
        //-------------PRE PROCESS-----------------------//
        std::string img_path = imgRoot + "\\" + name;
        PicPre img(img_path, inputFlag);
        img.Resize(netinfo.input_hw, PicPre::LONG_SIDE).rPad();
        //-------------SIM RUN-----------------------//
        auto im = netinfo.maketensor(img.dst_img);
        auto img_tensor = im.get();
        std::vector<icraft::Tensor> img_tensor_list = { *img_tensor };
        //屏蔽simulate内部打印
        freopen("sim_progress.bin", "w", stdout);
        freopen("sim_progress.bin", "w", stderr);
        auto result_tensor = network_ptr->run(img_tensor_list);
        //auto result_tensor_orig = network_ptr->run(img_path);
        /*std::vector<icraft::Tensor> result_tensor;
        for (auto&& result_tensor_ : result_tensor_orig) {
            result_tensor.push_back(result_tensor_.clone());
        }*/
        freopen("CON", "w", stdout);
        freopen("CON", "w", stderr);
        //-------------POST PROCESS-----------------------//
        netinfo.dataFormatH2S(result_tensor);
        auto yolo = (YoloSoft*)ObjectFactory::GetInstance()->CreateObject(classname);
        yolo->init(cfg, netinfo);
        //yolo->init_debug();
        yolo->filter(result_tensor);
        yolo->nms();
        yolo->coordTrans(img);
        if (show) {
            yolo->visualize(img.src_img, names);
        }
        yolo->saveRes(resRoot, name);
        delete yolo;
    }
    network_ptr->freeMem();
}

//不加pad resize前处理
void ppyoloe(const YAML::Node& network, const YAML::Node& param, std::string classname, const int inputFlag) {

    //-------------icraft config-----------------------//
    std::string jsonPath = network["jsonPath"].as<std::string>();
    std::string rawPath = network["rawPath"].as<std::string>();
    std::string resRoot = network["resRoot"].as<std::string>();
    bool show = network["show"].as<bool>();

    checkDir(resRoot);
    //--------------network config-----------------------//
    Yolo::extCfg cfg;
    // post relate
    cfg.CONF = param["conf"].as<float>();
    cfg.IOU = param["iou"].as<float>();
    cfg.NOC = param["number_of_class"].as<int>();
    cfg.NOH = param["number_of_head"].as<int>();
    cfg.ANCHORS = param["anchors"].as<std::vector<std::vector<std::vector<float>>>>();
    cfg.MULTILABLE = param["multilable"].as<bool>();

    // dataset relate
    std::string imgRoot = param["imgRoot"].as<std::string>();
    std::string imgList = param["imgList"].as<std::string>();
    std::string names_path = param["names"].as<std::string>();
    auto names = toVector(names_path);

    //-------------SET SIMULATOR-----------------------//
    std::map < std::string, std::string > options;
    options["json"] = jsonPath;
    options["raw"] = rawPath;
    options["log_path"] = ".log/";

    //generate can not use cudamode
    //目前打开cudamode连续推理会出bug，等待修复
    options["cudamode"] = "false";
    options["fake_qf"] = "false";
    // options["dump_ftmp"] = "SFB";

    //default
    options["target"] = "BUYI";
    options["show"] = "false";
    options["save"] = "false";

    auto network_ptr = std::make_shared<icraft::sim::SimulatorNetwork>(options);
    NetInfo_sim netinfo(network_ptr);

    //------------- PICS -----------------------//
    int index = 0;
    auto namevector = toVector(imgList);
    int totalnum = namevector.size();
    for (auto name : namevector) {
        progress(index, totalnum);
        index++;
        //-------------PRE PROCESS-----------------------//
        std::string img_path = imgRoot + "\\" + name;
        PicPre img(img_path, inputFlag);
        img.Resize(netinfo.input_hw, PicPre::BOTH_SIDE);

        //-------------SIM RUN-----------------------//
        auto im = netinfo.maketensor(img.dst_img);
        auto img_tensor = im.get();
        std::vector<icraft::Tensor> img_tensor_list = { *img_tensor };
        //屏蔽simulate内部打印
        freopen("sim_progress.bin", "w", stdout);
        freopen("sim_progress.bin", "w", stderr);
        auto result_tensor = network_ptr->run(img_tensor_list);
        freopen("CON", "w", stdout);
        freopen("CON", "w", stderr);
        //auto result_tensor = network_ptr->run(img_path);
        //-------------POST PROCESS-----------------------//
        netinfo.dataFormatH2S(result_tensor);
        auto yolo = (YoloSoft*)ObjectFactory::GetInstance()->CreateObject(classname);
        yolo->init(cfg, netinfo);
        //yolo->init_debug();
        yolo->filter(result_tensor);
        yolo->nms();
        yolo->coordTrans(img);
        if (show) {
            yolo->visualize(img.src_img, names);
        }
        yolo->saveRes(resRoot, name);
        delete yolo;
    }
    network_ptr->freeMem();
}

void semanticSeg(const YAML::Node& network, const YAML::Node& param) {
    // 语义分割模型 DDRNet socket  
    //-------------icraft config-----------------------//
    std::string jsonPath = network["jsonPath"].as<std::string>();
    std::string rawPath = network["rawPath"].as<std::string>();
    std::string resRoot = network["resRoot"].as<std::string>();
    bool show = network["show"].as<bool>();
    checkDir(resRoot);

    // dataset relate
    std::string imgRoot = param["imgRoot"].as<std::string>();
    std::string imgList = param["imgList"].as<std::string>();
    std::string dataset_name = param["dataset_name"].as<std::string>();

    //-------------SET simulate-----------------------//
    std::map < std::string, std::string > options;
    options["json"] = jsonPath;
    options["raw"] = rawPath;
    options["log_path"] = ".log/";

    //generate can not use cudamode
    //目前打开cudamode连续推理会出bug，等待修复
    options["cudamode"] = "false";
    options["fake_qf"] = "false";
    // options["dump_ftmp"] = "SFB";

    //default
    options["target"] = "BUYI";
    options["show"] = "false";
    options["save"] = "false";

    auto network_ptr = std::make_shared<icraft::sim::SimulatorNetwork>(options);
    NetInfo_sim netinfo(network_ptr);

    //------------- PICS -----------------------//
    int index = 0;
    auto namevector = toVector(imgList);
    int totalnum = namevector.size();
    for (auto name : namevector) {
        progress(index, totalnum);
        index++;
        //-------------PRE PROCESS-----------------------//
        std::string img_path = imgRoot + "\\" + name;
        PicPre img(img_path);
        img.Resize(netinfo.input_hw, PicPre::BOTH_SIDE);

        //-------------sim RUN-----------------------//
        auto im = netinfo.maketensor(img.dst_img);
        auto img_tensor = im.get();
        std::vector<icraft::Tensor> img_tensor_list = { *img_tensor };
        //屏蔽simulate内部打印
        freopen("sim_progress.bin", "w", stdout);
        freopen("sim_progress.bin", "w", stderr);
        auto result_tensor = network_ptr->run(img_tensor_list);
        freopen("CON", "w", stdout);
        freopen("CON", "w", stderr);

        //-------------POST PROCESS-----------------------//

        // 硬件排布转换为软件排布
        netinfo.dataFormatH2S(result_tensor);
        auto dims = result_tensor[0].dims();
        cv::Mat net_result_mat = cv::Mat(dims[1], dims[2], CV_32FC(dims[3]), result_tensor[0].data<float>().get());
        cv::resize(net_result_mat, net_result_mat, { img.dst_img.cols, img.dst_img.rows }, cv::INTER_LINEAR);
        cv::Mat img_id_mat;
        cv::Mat img_color_mat;
        ddrnet_map_plot(net_result_mat, img_id_mat, img_color_mat,dataset_name,img.dst_img);

        if (show) {
            cv::imshow("results", img_color_mat);
            cv::waitKey(0);
        }
        // save
        cv::imwrite(resRoot + "//" + name, img_id_mat);
    }
    network_ptr->freeMem();
}

void rdn(const YAML::Node& network, const YAML::Node& param) {
    // 超分辨模型 RDN socket
    //-------------icraft config-----------------------//
    std::string jsonPath = network["jsonPath"].as<std::string>();
    std::string rawPath = network["rawPath"].as<std::string>();
    std::string resRoot = network["resRoot"].as<std::string>();
    bool show = network["show"].as<bool>();
    checkDir(resRoot);

    // dataset relate
    std::string imgRoot = param["imgRoot"].as<std::string>();
    std::string imgList = param["imgList"].as<std::string>();

    std::map < std::string, std::string > options;
    options["json"] = jsonPath;
    options["raw"] = rawPath;
    options["log_path"] = ".log/";

    //generate can not use cudamode
    //目前打开cudamode连续推理会出bug，等待修复
    options["cudamode"] = "false";
    options["fake_qf"] = "false";
    // options["dump_ftmp"] = "SFB";

    //default
    options["target"] = "BUYI";
    options["show"] = "false";
    options["save"] = "false";

    auto network_ptr = std::make_shared<icraft::sim::SimulatorNetwork>(options);
    NetInfo_sim netinfo(network_ptr);
    

    //------------- PICS -----------------------//
    int index = 0;
    auto namevector = toVector(imgList);
    int totalnum = namevector.size();
    for (auto name : namevector) {
        progress(index, totalnum);
        index++;
        //-------------PRE PROCESS-----------------------//
        std::string img_path = imgRoot + "\\" + name;
        PicPre img(img_path);
        img.Resize(netinfo.input_hw, PicPre::BOTH_SIDE);

        //-------------sim RUN-----------------------//
        auto im = netinfo.maketensor(img.dst_img);
        auto img_tensor = im.get();
        std::vector<icraft::Tensor> img_tensor_list = { *img_tensor };
        //屏蔽simulate内部打印
        freopen("sim_progress.bin", "w", stdout);
        freopen("sim_progress.bin", "w", stderr);
        auto result_tensor = network_ptr->run(img_tensor_list);
        freopen("CON", "w", stdout);
        freopen("CON", "w", stderr);

        //-------------POST PROCESS-----------------------//

        // 硬件排布转换为软件排布
        netinfo.dataFormatH2S(result_tensor);

        auto dims = result_tensor[0].dims();

        cv::Mat net_result_mat = cv::Mat(dims[1], dims[2], CV_32FC3, result_tensor[0].data<float>().get());
        cv::cvtColor(net_result_mat, net_result_mat, cv::COLOR_RGB2BGR);
        if (show) {
            cv::imshow("results", net_result_mat);
            cv::waitKey(0);
        }
        // save
        //cv::imwrite(resRoot + name, net_result_mat * 255);      
    }
    network_ptr->freeMem();
}

void classify(const YAML::Node& network, const YAML::Node& param, const int inputFlag) {
    // 三通道图片分类模型 socket
    //-------------icraft config-----------------------//
    std::string jsonPath = network["jsonPath"].as<std::string>();
    std::string rawPath = network["rawPath"].as<std::string>();
    std::string resRoot = network["resRoot"].as<std::string>();
    bool show = network["show"].as<bool>();
    checkDir(resRoot);

    // dataset relate
    std::string imgRoot = param["imgRoot"].as<std::string>();
    std::string imgList = param["imgList"].as<std::string>();
    std::string names_path = param["names"].as<std::string>();
    std::pair<int, int> resize_hw = param["resize_hw"].as<std::pair<int, int>>();
    std::pair<int, int> crop_hw = param["crop_hw"].as<std::pair<int, int>>();

    //-------------SET simulate-----------------------//
    std::map < std::string, std::string > options;
    options["json"] = jsonPath;
    options["raw"] = rawPath;
    options["log_path"] = ".log/";

    //generate can not use cudamode
    //目前打开cudamode连续推理会出bug，等待修复
    options["cudamode"] = "false";
    options["fake_qf"] = "false";
    // options["dump_ftmp"] = "SFB";

    //default
    options["target"] = "BUYI";
    options["show"] = "false";
    options["save"] = "false";

    auto network_ptr = std::make_shared<icraft::sim::SimulatorNetwork>(options);
    NetInfo_sim netinfo(network_ptr);


    //------------- PICS -----------------------//
    int index = 0;
    auto namevector = toVector(imgList);
    int totalnum = namevector.size();
    for (auto name : namevector) {
        progress(index, totalnum);
        index++;
        //-------------PRE PROCESS-----------------------//
        std::string img_path = imgRoot + "\\" + name;

        PicPre img(img_path, inputFlag);
        img.Resize(resize_hw, PicPre::SHORT_SIDE).rCenterCrop(crop_hw);
        //-------------sim RUN-----------------------//
        auto im = netinfo.maketensor(img.dst_img);
        auto img_tensor = im.get();
        std::vector<icraft::Tensor> img_tensor_list = { *img_tensor };
        //屏蔽simulate内部打印
        freopen("sim_progress.bin", "w", stdout);
        freopen("sim_progress.bin", "w", stderr);
        auto result_tensor = network_ptr->run(img_tensor_list);
        freopen("CON", "w", stdout);
        freopen("CON", "w", stderr);

        //-------------POST PROCESS-----------------------//
        // 硬件排布转换为软件排布
        netinfo.dataFormatH2S(result_tensor);
        auto results = result_tensor[0].data<float>().get();
        auto res = softmax(results, 0, result_tensor[0].size());
        auto topk_res = topK(res, 5);
      
        classify_save(resRoot, name, topk_res);

        if (show) {
            classify_show(img.src_img, names_path, topk_res);
        }
    }
    network_ptr->freeMem();
}

void byteTrack(const YAML::Node& network, const YAML::Node& param, std::string classname) {
    //-------------icraft config-----------------------//
    std::string jsonPath = network["jsonPath"].as<std::string>();
    std::string rawPath = network["rawPath"].as<std::string>();
    std::string resRoot = network["resRoot"].as<std::string>();
    bool show = network["show"].as<bool>();
    checkDir(resRoot);
    //--------------network config-----------------------//
    Yolo::extCfg cfg;
    // post relate
    cfg.CONF = param["conf"].as<float>();
    cfg.IOU = param["iou"].as<float>();
    cfg.NOC = param["number_of_class"].as<int>();
    cfg.NOH = param["number_of_head"].as<int>();
    cfg.ANCHORS = param["anchors"].as<std::vector<std::vector<std::vector<float>>>>();
    cfg.MULTILABLE = param["multilable"].as<bool>();

    // dataset relate
    std::string videoRoot = param["videoRoot"].as<std::string>();
    std::string videoList = param["videoList"].as<std::string>();
    std::string names_path = param["names"].as<std::string>();
    auto names = toVector(names_path);

    //-------------SET SIMULATOR-----------------------//
    std::map < std::string, std::string > options;
    options["json"] = jsonPath;
    options["raw"] = rawPath;
    options["log_path"] = ".log/";

    //generate can not use cudamode
    //目前打开cudamode连续推理会出bug，等待修复
    options["cudamode"] = "false";
    options["fake_qf"] = "false";
    // options["dump_ftmp"] = "SFB";

    //default
    options["target"] = "BUYI";
    options["show"] = "false";
    options["save"] = "false";

    auto network_ptr = std::make_shared<icraft::sim::SimulatorNetwork>(options);
    NetInfo_sim netinfo(network_ptr);

    //------------- PICS -----------------------//
    int index = 0;
    auto namevector = toVector(videoList);
    int totalnum = namevector.size();
    for (auto name : namevector) {
        //-------------PRE PROCESS-----------------------//
        std::string video_path = videoRoot + "\\" + name;
        cv::VideoCapture cap(video_path);
        cv::Mat one_frame;
        int fps = cap.get(cv::CAP_PROP_FPS);
        BYTETracker tracker(fps, 30);
        std::vector<std::array<float, 10>> res_export = std::vector<std::array<float, 10>>();
        auto total_frames = cap.get(cv::CAP_PROP_FRAME_COUNT);
        for (int frame_id = 0; frame_id < total_frames; ++frame_id) {
            progress(frame_id, total_frames);
            cap >> one_frame;
            PicPre img(one_frame);
            img.Resize(netinfo.input_hw, PicPre::LONG_SIDE).rPad();
            //-------------ICRAFT SIM RUN-----------------------//
            auto im = netinfo.maketensor(img.dst_img);
            auto img_tensor = im.get();
            std::vector<icraft::Tensor> img_tensor_list = { *img_tensor };
            //屏蔽simulate内部打印
            freopen("sim_progress.bin", "w", stdout);
            freopen("sim_progress.bin", "w", stderr);
            auto result_tensor = network_ptr->run(img_tensor_list);
            freopen("CON", "w", stdout);
            freopen("CON", "w", stderr);
            //-------------POST PROCESS-----------------------//
            netinfo.dataFormatH2S(result_tensor);
            auto yolo = (YoloSoft*)ObjectFactory::GetInstance()->CreateObject(classname);
            yolo->init(cfg, netinfo);
            //yolo->init_debug();
            yolo->filter(result_tensor);
            yolo->nms();
            yolo->coordTrans(img, false);
            //yolo->visualize(img.src_img, names);
            auto res = yolo->getOutputData();
            std::vector<Object> objects;
            for (auto i : res) {
                Object obj;
                obj.rect.x = i[1];
                obj.rect.y = i[2];
                obj.rect.width = i[3];
                obj.rect.height = i[4];
                obj.label = 0;
                obj.prob = i[5];
                objects.push_back(obj);
            }
            std::vector<STrack> output_stracks = tracker.update(objects);

            for (int i = 0; i < output_stracks.size(); i++) {
                auto frame = (float)(frame_id)+1;
                auto id = (float)(output_stracks[i].track_id);
                std::vector<float> tlwh = output_stracks[i].tlwh;
                bool vertical = tlwh[2] / tlwh[3] > 1.6;
                if (tlwh[2] * tlwh[3] > 20 && !vertical) {
                    auto bb_left = tlwh[0];
                    auto bb_top = tlwh[1];
                    auto bb_width = tlwh[2];
                    auto bb_height = tlwh[3];
                    auto conf = output_stracks[i].score;
                    std::array<float, 10> each_line = { frame, id, bb_left, bb_top, bb_width, bb_height, conf, -1.f, -1.f, -1.f };
                    res_export.push_back(each_line);
                    if (show) {
                        Scalar s = tracker.get_color(output_stracks[i].track_id);
                        putText(one_frame, format("%.0f", id), Point(bb_left, bb_top), 0, 0.6, Scalar(0, 0, 255), 2, LINE_AA);
                        cv::rectangle(one_frame, Rect(bb_left, bb_top, bb_width, bb_height), s, 2);
                    }
                }
            }
            if (show) {
                cv::imshow(name, one_frame);
                cv::waitKey(1);
            }
            delete yolo;
        }

        // save
        std::string result_file = resRoot + "\\" + name + ".txt";
        std::fstream outFile(result_file, std::ios::out | std::ios::trunc);
        for (size_t line = 0; line < res_export.size(); ++line) {
            for (size_t i = 0; i < 10; ++i) {
                outFile << res_export[line][i];
                if (i != 9) {
                    outFile << ",";
                }
            }
            outFile << "\n";
        }
        outFile.close();

        progress(index, totalnum);
        index++;
    }
    network_ptr->freeMem();
}

void saliencyDet(const YAML::Node& network, const YAML::Node& param) {
    std::string jsonPath = network["jsonPath"].as<std::string>();
    std::string rawPath = network["rawPath"].as<std::string>();
    std::string resRoot = network["resRoot"].as<std::string>();
    bool show = network["show"].as<bool>();
    checkDir(resRoot);

    // dataset relate
    std::string imgRoot = param["imgRoot"].as<std::string>();
    std::string imgList = param["imgList"].as<std::string>();

    std::map < std::string, std::string > options;
    options["json"] = jsonPath;
    options["raw"] = rawPath;
    options["log_path"] = ".log/";

    //generate can not use cudamode
    //目前打开cudamode连续推理会出bug，等待修复
    options["cudamode"] = "false";
    options["fake_qf"] = "false";
    // options["dump_ftmp"] = "SFB";

    //default
    options["target"] = "BUYI";
    options["show"] = "false";
    options["save"] = "false";

    auto network_ptr = std::make_shared<icraft::sim::SimulatorNetwork>(options);
    NetInfo_sim netinfo(network_ptr);


    //------------- PICS -----------------------//
    int index = 0;
    auto namevector = toVector(imgList);
    int totalnum = namevector.size();
    for (auto name : namevector) {
        progress(index, totalnum);
        index++;
        //-------------PRE PROCESS-----------------------//
        std::string img_path = imgRoot + "\\" + name;
        PicPre img(img_path);
        img.Resize(netinfo.input_hw, PicPre::BOTH_SIDE);

        //-------------sim RUN-----------------------//
        auto im = netinfo.maketensor(img.dst_img);
        auto img_tensor = im.get();
        std::vector<icraft::Tensor> img_tensor_list = { *img_tensor };
        //屏蔽simulate内部打印
        freopen("sim_progress.bin", "w", stdout);
        freopen("sim_progress.bin", "w", stderr);
        auto result_tensor = network_ptr->run(img_tensor_list);
        freopen("CON", "w", stdout);
        freopen("CON", "w", stderr);

        //-------------POST PROCESS-----------------------//

        // 硬件排布转换为软件排布
        netinfo.dataFormatH2S(result_tensor);
        auto dims = result_tensor[0].dims();
        auto size = result_tensor[0].size();
        float* data = result_tensor[0].data<float>().get();
        sigmoid(data, size,true);
        //cv::Mat net_result_mat = cv::Mat(dims[1], dims[2], CV_32FC3, result_tensor[0].data<float>().get());
        cv::Mat net_result_mat = cv::Mat(dims[1], dims[2], CV_32FC1, data);
        //cv::cvtColor(net_result_mat, net_result_mat, cv::COLOR_RGB2BGR);

        net_result_mat = net_result_mat * 255;
        auto d = img.src_dims;
        auto h = std::get<1>(d);
        auto w = std::get<2>(d);
        cv::Mat resultMat;
        cv::resize(net_result_mat, resultMat, cv::Size(w, h), 0, 0, cv::INTER_LINEAR);

        if (show) {
            cv::imshow("results", resultMat);
            cv::waitKey(0);
        }
        // save
        cv::imwrite(resRoot + "//" + name, resultMat);
    }
    network_ptr->freeMem();
}