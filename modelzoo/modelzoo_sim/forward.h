#pragma once
#include "yaml-cpp/yaml.h"
#include "opencv2/opencv.hpp"
#include "runtime.h"
#include "PicPre.hpp"

#define FFYOLO(classname)\
static void classname##_ff(const YAML::Node& network, const YAML::Node& param) {\
	return yolo(network, param, #classname);\
}\

void yolo(const YAML::Node& network, const YAML::Node& param, std::string classname,
	int inputFlag = PicPre::ImreadModes::IMREAD_COLOR);
void semanticSeg(const YAML::Node& network, const YAML::Node& param);

void rdn(const YAML::Node& network, const YAML::Node& param);

void classify(const YAML::Node& network, const YAML::Node& param, const int inputFlag);

static void classify_color(const YAML::Node& network, const YAML::Node& param) {
	return classify(network, param, PicPre::ImreadModes::IMREAD_COLOR);
};

void ppyoloe(const YAML::Node& network, const YAML::Node& param, std::string classname,
	int inputFlag = PicPre::ImreadModes::IMREAD_COLOR);

static void PPYoloE_sim_ff(const YAML::Node& network, const YAML::Node& param) {
	return ppyoloe(network, param, "PPYoloE_sim");
}

void byteTrack(const YAML::Node& network, const YAML::Node& param, std::string classname);
static void ByteTrack_sim_ff(const YAML::Node& network, const YAML::Node& param) {
	return byteTrack(network, param, "YoloX_sim");
}

void saliencyDet(const YAML::Node& network, const YAML::Node& param);

/*
* 函数注册：
* 由于函数与类存在依赖关系，在对应的类hpp中用工厂的方式注册函数指针map会引起交叉引用，
* 目前还没有花时间解决这个问题，只能先在这边手动注册
*/
FFYOLO(YoloV5_sim);
FFYOLO(YoloV3_sim);
FFYOLO(YoloV6_sim);
FFYOLO(YoloV8_sim);
FFYOLO(YoloX_sim);
FFYOLO(YoloV5_pose_sim);
FFYOLO(YoloV5_obb_sim);
FFYOLO(YoloV5_seg_sim);
FFYOLO(YoloV8_pose_sim);

/*
* 添加说明：
①string部分可以自定义，要与yaml中调用的名字一致
②函数指针的名字，由FFYOLO(classname)中的类名决定，既:classname##_ff
*/
using model_forward = void(*) (const YAML::Node& network, const YAML::Node& param);
static std::map<std::string, model_forward>  funcMap = {
	{"yolov5_sim",YoloV5_sim_ff},
	{"yolov3_sim",YoloV3_sim_ff},
	{"yolov6_sim",YoloV6_sim_ff},
	{"yolov8_sim",YoloV8_sim_ff},
	{"yolox_sim",YoloX_sim_ff},
	{"ppyoloe_sim",PPYoloE_sim_ff},
	{"yolov5_pose_sim",YoloV5_pose_sim_ff},
	{"yolov8_pose_sim",YoloV8_pose_sim_ff},
	{"yolov5_obb_sim",YoloV5_obb_sim_ff},
	{"yolov5_seg_sim",YoloV5_seg_sim_ff},
	{"semanticSeg_sim",semanticSeg},
	{"rdn_sim",rdn},
	{"classify_color_sim",classify_color},
	{"bytetrack_sim",ByteTrack_sim_ff},
	{"saliencyDet_sim",saliencyDet},
};

