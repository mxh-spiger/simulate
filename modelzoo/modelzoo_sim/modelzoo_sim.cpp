﻿#include "forward.h"
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <windows.h>
#include "yaml-cpp/yaml.h"
#include "utils.hpp"

int main(int argc, char** argv) {
	YAML::Node config = YAML::LoadFile(argv[1]);
	auto models = config["hierarchy"]["models"].as<std::vector<std::vector<std::string>>>();
	auto networks = config["networks"];
	auto params = config["params"];
	int model_no = 0;
	int total = models.size();
	for (auto model : models) {
		model_no++;
		/*try {
			auto func_name = model[0];
			auto network = networks[model[1]];
			auto param = params[model[2]];
			std::cout << "\n--------------------------------------\n NO. "
				<< model_no << "/" << total << " : " << model[1] << " \n"
				<< "--------------------------------------\n"
				<< network
				<< "\n--------------------------------------" << std::endl;
			funcMap[func_name](network, param);
		}
		catch (std::exception e) {
			std::cout << "\n**************\nerror model: " << model[1] << std::endl;
			std::cout << e.what() << std::endl;
			std::cout << "**************\n";
		}*/

		auto func_name = model[0];
		auto network = networks[model[1]];
		auto param = params[model[2]];
		std::cout << "\n--------------------------------------\n NO. "
			<< model_no << "/" << total << " : " << model[1] << " \n"
			<< "--------------------------------------\n"
			<< network
			<< "\n--------------------------------------" << std::endl;
		funcMap[func_name](network, param);


	}

	return 0;
}

