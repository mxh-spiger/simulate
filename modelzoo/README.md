

# MODEL ZOO 使用说明

## 一、简介

本工程主要包含3部分：`nnio`、`modelzoo_sim`、`modelzoo_socket`

1. `nnio`: 主要包括3部分：

   - 前处理

   - 后处理

   - 网络信息获取；输入输出数据处理

2. `modelzoo_socket`为socekt链接模式的上板前向运行时工程；
3. `modelzoo_sim`为simulate仿真前向；🚩目前仅支持不带硬算子仿真，请编译时去掉`[config]`部分（不影响精度和最终结果）；

## 二、环境准备

使用前请检查一下几点：

1. 请将thirdparty依赖包与本解决方案文件夹置于同级

   ![image-20230419213122996](assets/image-20230419213122996.png)

2. **确保为release模式**

![image-20220808133905146](assets/image-20220808133905146-16599375310272.png)

3. **确认语言标准为c++ 17**

![image-20220808134120375](assets/image-20220808134120375.png)

4. **配置属性-调试修改：**

   - 设置配置文件路径：`socket`提供了`demo.yaml`例子，`simulate`提供了`sim.yaml`例子；

   - 将thirdparty的dll加入环境中，dll主要用了yaml，simulate等
   
     <img src="assets/image-20230403191009073.png" alt="	" style="zoom: 80%;" />
   
   **socket:**

​		环境：	`PATH=%PATH%;../../thirdparty/dll;`   			

​		**sim:**

​		环境： `PATH=%PATH%;../../thirdparty/dll;C:\Icraft-CLI\Rely;`

5. 编译过程提示找不到ir virsion相关头文件，可以将那行include注释掉

## 三、使用方法

yaml文件主要包括3部分

model部分需要配置要跑的网络模型

通过一个list:`[func, network, param]`来配置model前向函数，jr路径，模型相关参数等

- func：名称要与forward.h中的func map中的字符串一致

- network：需要配置jrpath；结果保存路径（🚩需要注意的是，该路径的次级路径必须存在，代码仅帮助会生成末尾文件夹）；

- param：需要配置输入数据集信息；前后处理参数等，可以参考已有模型的配置；

  

## 四、开发方法

1. 如果要新增模型前向函数，怎需要在forward.cpp增加函数，并在forward.h的函数map中注册该函数；

2. nnio中提供了若干功能类，可以用于新模型后处理开发参考

   - 前处理类：

   <img src="assets/image-20230419220550132.png" alt="image-20230419220550132" style="zoom: 50%;" />

   - 网络信息处理：

     负责：

     1. 从json&raw中获取必要的网络信息：例如bit数，用了哪些硬算子，输入输出维度、量化系数等等
     2. 处理输入、输出数据；输入：构造icraft相关tensor；输出：将得到的icraft相关tensor转成普通数据类型指针；

     下图左边是用于仿真，右边用于socekt运行时

     <img src="assets/image-20230419220659006.png" alt="image-20230419220659006" style="zoom:50%;" />

     

   - 后处理：

     yolo系列模型较多，下面是其继承关系图

     基类为yolo[abstract]，派生了

     - yolohard[abstract]：派生出若干模型带硬算子的socekt后处理；

     - yolosoft[abstract]：派生出若干模型不带硬算子的simulate后处理（暂未提供yolo系列不带硬算子的socket后处理）；

     

     

     ![image-20230419221745815](assets/image-20230419221745815.png)

   - 其他：

     由于yolo系列网络我们接触较多，且后处理比较灵活种类较多，使用工厂类注册比较方便

     <img src="assets/image-20230419221416162.png" alt="image-20230419221416162" style="zoom: 50%;" />



## 五、声明

🚩本工程仅作为研究、参考；受开发条件限制，有些模型前后处理未做严谨的测试验证，请勿未经验证直接用于项目开发。