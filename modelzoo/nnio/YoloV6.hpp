#pragma once
#include "YoloHard.hpp"
#include "factory.hpp"


class  YoloV6 :public YoloHard {
public:
	YoloV6() {}
	~YoloV6() {}
	template<typename T>
	float grs(T* tensor_data,
		int obj_ptr_start, std::vector<float> norm, int index) {
		auto _prob_ = sigmoid(tensor_data[obj_ptr_start + index] * norm[0]);
		return _prob_;
	}

	template<typename T>
	std::pair<int, float> gmrs(T* tensor_data, int obj_ptr_start, std::vector<float> norm) {
		T* class_ptr_start = tensor_data + obj_ptr_start;
		T* max_prob_ptr = std::max_element(class_ptr_start, class_ptr_start + this->NOC);
		int max_index = std::distance(class_ptr_start, max_prob_ptr);
		auto _prob_ = sigmoid(*max_prob_ptr * norm[0]);
		return { max_index, _prob_ };
	}

	template<typename T>
	std::vector<float> gb(T* tensor_data,
		std::vector<float> norm, int obj_ptr_start, int location_x, int location_y,
		int stride, std::vector<float> anchor) {

		std::vector<float> ltrb;
		int box_ptr = obj_ptr_start + this->real_out_channles[0];
		for (size_t i = 0; i < 4; i++)
		{
			ltrb.push_back((tensor_data[box_ptr + i])* norm[1]);
		};

		float x = ((ltrb[2] - ltrb[0]) * 0.5 + location_x + 0.5) * stride;
		float y = ((ltrb[3] - ltrb[1]) * 0.5 + location_y + 0.5) * stride;
		float w = (ltrb[0] + ltrb[2]) * stride;
		float h = (ltrb[1] + ltrb[3]) * stride;

		std::vector<float> xywh = { x,y,w,h };
		return xywh;
	}


	float getRealScore(int8_t* tensor_data,
		int obj_ptr_start, std::vector<float> norm, int index) {

		auto s = (this->bits == 16) ? this->grs((int16_t*)tensor_data, obj_ptr_start
			, norm, index) : this->grs(tensor_data, obj_ptr_start, norm, index);

		return s;
	}


	std::pair<int, float> getMaxRealScore(int8_t* tensor_data, int obj_ptr_start, std::vector<float> norm) {

		auto s = (this->bits == 16) ? this->gmrs((int16_t*)tensor_data, obj_ptr_start
			, norm) : this->gmrs(tensor_data, obj_ptr_start, norm);

		return s;
	}


	std::vector<float> getBbox(int8_t* tensor_data,
		std::vector<float> norm, int obj_ptr_start, int location_x, int location_y,
		int stride, std::vector<float> anchor) {
		auto xywh = (this->bits == 16) ? this->gb((int16_t*)tensor_data, norm
			, obj_ptr_start, location_x, location_y, stride, anchor) : this->gb(tensor_data, norm
				, obj_ptr_start, location_x, location_y, stride, anchor);
		return xywh;
	}


protected:
	void set_ori_out_channles() {
		this->ori_out_channles = { this->NOC,4 };
	}
};

REGISTERPANELCLASS(YoloV6);