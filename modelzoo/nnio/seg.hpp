#include "configure.hpp"
#include "opencv2/opencv.hpp"
void ddrnet_map_plot(const cv::Mat& src_mat, cv::Mat& img_id_mat, cv::Mat& img_color_mat,std::string dataset_name,const cv::Mat& dst_img) {
	//------------ Finds id of every pixels ----------------//
	std::vector<std::vector<uint8_t>> PALETTE = palette_map[dataset_name];
	const int channels = src_mat.channels();
	int index;
	std::vector<int> img_id;
	for (int row = 0; row < src_mat.rows; row++) {
		const float* pointer = src_mat.ptr<float>(row);
		for (int col = 0; col < src_mat.cols; col++) {
			index = std::distance(pointer + col * channels, std::max_element(pointer + col * channels, pointer + (col + 1) * channels));
			img_id.emplace_back(index);
		}
	}
	img_id_mat = cv::Mat(img_id).clone();
	img_id_mat = img_id_mat.reshape(1, src_mat.rows);

	//------------- uses palette for coloring  ------------//
	std::vector<uint8_t> img_color;
	for (auto i : img_id) {
		img_color.emplace_back(PALETTE[i][2]);
		img_color.emplace_back(PALETTE[i][1]);
		img_color.emplace_back(PALETTE[i][0]);
	}
	img_color_mat = cv::Mat(img_color).clone();
	img_color_mat = img_color_mat.reshape(3, src_mat.rows);
	cv::Mat src;
	dst_img.convertTo(src, CV_8UC3);
	cv::scaleAdd(img_color_mat, 0.8, src, img_color_mat);
}
