#pragma once
#include "YoloHard.hpp"
#include "factory.hpp"


class  YoloV8 :public YoloHard {
public:
	YoloV8() {}
	~YoloV8() {}
	template<typename T>
	float grs(T* tensor_data,
		int obj_ptr_start, std::vector<float> norm, int index) {
		auto _prob_ = sigmoid(tensor_data[obj_ptr_start + index] * norm[0]);
		return _prob_;
	}

	template<typename T>
	std::pair<int, float> gmrs(T* tensor_data, int obj_ptr_start, std::vector<float> norm) {
		T* class_ptr_start = tensor_data + obj_ptr_start;
		T* max_prob_ptr = std::max_element(class_ptr_start, class_ptr_start + this->NOC);
		int max_index = std::distance(class_ptr_start, max_prob_ptr);
		auto _prob_ = sigmoid(*max_prob_ptr * norm[0]);
		return { max_index, _prob_ };
	}

	template<typename T>
	std::vector<float> gb(T* tensor_data,
		std::vector<float> norm, int obj_ptr_start, int location_x, int location_y,
		int stride, std::vector<float> anchor) {

		std::vector<float> ltrb = dfl(tensor_data, 
			norm[1], obj_ptr_start + this->real_out_channles[0],this->bbox_info_channel);
		float x1 = location_x + 0.5 - ltrb[0];
		float y1 = location_y + 0.5 - ltrb[1];
		float x2 = location_x + 0.5 + ltrb[2];
		float y2 = location_y + 0.5 + ltrb[3];

		float x = ((x2 + x1) / 2.f) * stride;
		float y = ((y2 + y1) / 2.f) * stride;
		float w = (x2 - x1) * stride;
		float h = (y2 - y1) * stride;

		std::vector<float> xywh = { x,y,w,h };
		return xywh;
	}


	float getRealScore(int8_t* tensor_data,
		int obj_ptr_start, std::vector<float> norm, int index) {

		auto s = (this->bits == 16) ? this->grs((int16_t*)tensor_data, obj_ptr_start
			, norm, index) : this->grs(tensor_data, obj_ptr_start, norm, index);

		return s;
	}


	std::pair<int, float> getMaxRealScore(int8_t* tensor_data, int obj_ptr_start, std::vector<float> norm) {

		auto s = (this->bits == 16) ? this->gmrs((int16_t*)tensor_data, obj_ptr_start
			, norm) : this->gmrs(tensor_data, obj_ptr_start, norm);

		return s;
	}


	std::vector<float> getBbox(int8_t* tensor_data,
		std::vector<float> norm, int obj_ptr_start, int location_x, int location_y,
		int stride, std::vector<float> anchor) {
		auto xywh = (this->bits == 16) ? this->gb((int16_t*)tensor_data, norm
			, obj_ptr_start, location_x, location_y, stride, anchor) : this->gb(tensor_data, norm
				, obj_ptr_start, location_x, location_y, stride, anchor);
		return xywh;
	}


protected:
	int bbox_info_channel;
	virtual void set_ori_out_channles() {
		this->bbox_info_channel = 64;
		this->ori_out_channles = { this->NOC,this->bbox_info_channel };
	}
	
};

REGISTERPANELCLASS(YoloV8);