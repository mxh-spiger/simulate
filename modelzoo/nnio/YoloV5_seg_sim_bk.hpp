#pragma once
#include "YoloSoft.hpp"
#include "factory.hpp"
#include <Eigen/Core>
#include <Eigen/Dense>

using namespace std::chrono;

class  YoloV5_seg_sim :public YoloSoft {
public:
	YoloV5_seg_sim() {}
	~YoloV5_seg_sim() {}

	virtual void getPtr(std::vector<std::shared_ptr<icraft::Tensor>> one_head_ptrs) override {
		float* tensor_data = one_head_ptrs[0]->data<float>().get();
		this->boxPtr = tensor_data + ptrLocate(this->NOC + 5 + this->mask_channel);
		this->scorePtr = this->boxPtr + 4;
		this->classPtr = this->boxPtr + 5;
	};

	virtual std::pair<int, float> getMaxRealScore() override {
		auto _score_ = sigmoid(*this->scorePtr);
		float* max_prob_ptr = std::max_element(this->classPtr, this->classPtr + this->NOC);
		int max_index = std::distance(this->classPtr, max_prob_ptr);
		auto _prob_ = sigmoid(*max_prob_ptr);
		return { max_index,_score_ * _prob_ };
	}

	virtual float getRealScore(int index) override {
		float* probPtr = this->classPtr + index;
		auto _score_ = sigmoid(*this->scorePtr);
		auto _prob_ = sigmoid(*probPtr);
		return _score_ * _prob_;
	}

	virtual std::vector<float> getBbox(int stride, std::vector<float> anchor) override {
		auto xywh = S1::_V5(this->boxPtr);
		S2::_V5(xywh, this->_w, this->_h, stride, anchor);
		float* seg_ptr_start = classPtr + this->NOC;
		std::vector<float> one_obj_mask_info;
		for (size_t i = 0; i < this->mask_channel; i++)
		{
			one_obj_mask_info.push_back(*(seg_ptr_start + i));
		}
		this->mask_info.push_back(one_obj_mask_info);
		return xywh;
	}

	void nms() {
		auto bbox_num = this->id_list.size();
		for (int i = 0; i < bbox_num; i++)
		{
			this->seg_filter_res.push_back({ this->id_list[i] ,this->socre_list[i],
				this->box_list[i],this->mask_info[i] });
		}

		std::stable_sort(this->seg_filter_res.begin(), this->seg_filter_res.end(),
			[](const YOLO_SEG_RES& tuple1, const YOLO_SEG_RES& tuple2) {
				return std::get<1>(tuple1) > std::get<1>(tuple2);
			}
		);
		int idx = 0;
		for (auto res : this->seg_filter_res) {
			bool keep = true;
			for (int k = 0; k < this->seg_nms_res.size() && keep; ++k) {
				if (std::get<0>(res) == std::get<0>(this->seg_nms_res[k])) {
					if (1.f - jaccardDistance(std::get<2>(res), std::get<2>(this->seg_nms_res[k])) > this->IOU) {
						keep = false;
					}
				}
			}
			if (keep == true)
				this->seg_nms_res.push_back(res);
			if (idx > this->max_nms) {
				break;
			}
			idx++;
		}

	}

	void coordTrans(PicPre& img, bool check_border = true) {
		int left_pad = img.getPad().first;
		int top_pad = img.getPad().second;
		float ratio = img.getRatio().first;
		auto proto = this->results[this->NOH][0];
		float* protodata = proto->data<float>().get();
		//saveFtmp(protodata, 160 * 160 * 32, "./mask.ftmp");
		int obj_idn = 0;
		for (auto&& res : this->seg_nms_res) {
			float class_id = std::get<0>(res);
			float score = std::get<1>(res);
			auto box = std::get<2>(res);
			float x1 = (box.tl().x - left_pad) / ratio;
			float y1 = (box.tl().y - top_pad) / ratio;
			float x2 = (box.br().x - left_pad) / ratio;
			float y2 = (box.br().y - top_pad) / ratio;
			if (check_border) {
				x1 = checkBorder(x1, 0.f, (float)img.src_img.cols);
				y1 = checkBorder(y1, 0.f, (float)img.src_img.rows);
				x2 = checkBorder(x2, 0.f, (float)img.src_img.cols);
				y2 = checkBorder(y2, 0.f, (float)img.src_img.rows);
			}
			float w = x2 - x1;
			float h = y2 - y1;
			//bbox：左上角点和wh
			this->output_data.emplace_back(std::vector<float>({ class_id, x1, y1, w, h, score }));
			//计算mask
			std::vector<float> masks_in = std::get<3>(res);
			auto box_param_path = "box_param_" + std::to_string(obj_idn) + ".ftmp";
			//saveFtmp(masks_in, box_param_path);
			obj_idn++;
			std::vector<float> masks_out;
			auto protoc = this->mask_channel;
			//auto start = system_clock::now();
			for (size_t h = 0; h < this->protoh; h++) {
				auto ih = h * this->protow * protoc;
				for (size_t w = 0; w < this->protow; w++) {
					auto iw = w * protoc;
					float data = 0.f;
					for (size_t c = 0; c < protoc; c++) {
						float x = 0.f;
						int index = ih + iw + c;
						x = protodata[index];
						data += (masks_in[c] * x);
					}
					masks_out.emplace_back(sigmoid(data));
				}
			}
			this->allmask.emplace_back(masks_out);
		}

	}


	virtual void visualize(const cv::Mat& img, const std::vector<std::string>& names) {
		std::default_random_engine e;
		std::uniform_int_distribution<unsigned> u(10, 200);
		int index = 0;
		cv::Mat mask = cv::Mat::zeros(cv::Size(img.cols, img.rows), img.type());
		cv::Mat rgb_mask = cv::Mat::zeros(cv::Size(img.cols, img.rows), img.type());
		auto dst_size = (img.cols >= img.rows) ? img.cols : img.rows;
		auto mask_size = (this->protoh >= this->protoh) ? this->protoh : this->protoh;
		auto ratio = (float)dst_size / (float)mask_size;
		for (auto res : this->output_data) {
			int class_id = (int)res[0];
			float x1 = res[1];
			float y1 = res[2];
			float w = res[3];
			float h = res[4];
			float score = res[5];
			cv::Scalar color_ = cv::Scalar(u(e), u(e), u(e));
			cv::rectangle(img, cv::Rect2f(x1, y1, w, h), color_, 2);
			std::stringstream ss;
			ss << std::fixed << std::setprecision(2) << score;
			std::string s = std::to_string(class_id) + "_" + names[class_id] + " " + ss.str();
			auto s_size = cv::getTextSize(s, cv::FONT_HERSHEY_DUPLEX, 0.5, 1, 0);
			cv::rectangle(img, cv::Point2f(x1 - 1, y1 - s_size.height - 7), cv::Point2f(x1 + s_size.width, y1 - 2), color_, -1);
			cv::putText(img, s, cv::Point2f(x1, y1 - 2), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(255, 255, 255), 0.2);

			//draw mask
			auto masks_out = this->allmask[index];
			cv::Mat masks_1{ masks_out };
			cv::Mat masks_2;
			masks_1 = masks_1.reshape(1, this->protoh);
			cv::resize(masks_1, masks_1, cv::Size(this->protow * ratio, this->protoh * ratio));
			masks_2 = masks_1(cv::Range(0, img.rows), cv::Range(0, img.cols));
			cv::Mat mask_tmp = cv::Mat::zeros(cv::Size(img.cols, img.rows), CV_8UC1);
			// 将masked目标从原图上裁剪下来
			cv::Mat masks_3 = masks_2(cv::Range(y1, y1 + h), cv::Range(x1, x1 + w));
			// 将目标区域粘贴到原图的黑色背景上
			masks_3.copyTo(mask_tmp(cv::Range(y1, y1 + h), cv::Range(x1, x1 + w)));
			cv::Mat rgb_mask_test = cv::Mat::zeros(cv::Size(img.cols, img.rows), img.type());
			add(rgb_mask_test, color_, rgb_mask_test, mask_tmp);
			mask = mask + rgb_mask_test;
			index += 1;
		}
		cv::Mat out = img + mask * 0.5;
		cv::imshow("results", out);
		cv::waitKey(0);

	}

	virtual void saveRes(std::string resRoot, std::string name) {
		auto box_path = resRoot + "\\box";
		auto mask_path = resRoot + "\\mask";
		checkDir(box_path);
		checkDir(mask_path);

		std::string box_save_path = box_path + "\\" + name;
		std::string mask_save_path = mask_path + "\\" + name;

		std::regex reg(R"(\.(\w*)$)");
		box_save_path = std::regex_replace(box_save_path, reg, ".txt");
		mask_save_path = std::regex_replace(mask_save_path, reg, ".bin");

		std::ofstream outputFileB(box_save_path);
		std::ofstream outputFileM(mask_save_path, std::ios::out | std::ios::binary);

		if (!outputFileB.is_open()) {
			std::cout << "Create BOX txt file fail." << std::endl;
		}
		if (!outputFileM.is_open()) {
			std::cout << "Create MASK txt file fail." << std::endl;
		}

		int index = 0;
		for (auto i : this->output_data) {
			for (auto j : i) {
				outputFileB << j << " ";
			}
			outputFileB << "\n";
			auto masks_out = this->allmask[index];
			for (auto i : masks_out) {
				outputFileM.write((const char*)&i, sizeof(float));
			}
			index++;
		}
		outputFileB.close();
		outputFileM.close();
	}

protected:
	int protoh = 160;
	int protow = 160;
	int mask_stride = 4;
	int mask_channel = 32;
	std::vector<std::vector<float>> allmask;

	void set_ori_out_channles() {
		this->ori_out_channles = { 3 * (this->NOC + 5 + this->mask_channel) };

	}
	std::vector<std::vector<float>> mask_info;
	using YOLO_SEG_RES = std::tuple<int, float, cv::Rect2f, std::vector<float>>;
	std::vector<YOLO_SEG_RES> seg_filter_res, seg_nms_res;
	Eigen::Matrix<float, 32, Eigen::Dynamic> mask_kernel;

};

REGISTERPANELCLASS(YoloV5_seg_sim);