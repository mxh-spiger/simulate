#pragma once
#include "YoloSoft.hpp"
#include "factory.hpp"
#include "configure.hpp"


class  YoloV8_pose_sim :public YoloSoft {
public:
	YoloV8_pose_sim() {}
	~YoloV8_pose_sim() {}

	//先计算各部分数据的指针

	virtual void getPtr(std::vector<std::shared_ptr<icraft::Tensor>> one_head_ptrs) override {
		float* score_data = one_head_ptrs[0]->data<float>().get();
		float* box_data = one_head_ptrs[1]->data<float>().get();
		float* class_data = one_head_ptrs[2]->data<float>().get();

		this->scorePtr = score_data + ptrLocate(1);
		this->boxPtr = box_data + ptrLocate(this->bbox_info_channel);
		this->classPtr = class_data + ptrLocate(51);

	};

	virtual std::pair<int, float> getMaxRealScore() override {
		float* max_prob_ptr = std::max_element(this->scorePtr, this->scorePtr + this->NOC);
		int max_index = std::distance(this->scorePtr, max_prob_ptr);
		auto _prob_ = sigmoid(*max_prob_ptr);
		return { max_index, _prob_ };
	}

	virtual float getRealScore(int index) override {
		float* probPtr = this->scorePtr + index;
		auto _prob_ = sigmoid(*probPtr);
		return  _prob_;
	}

	virtual std::vector<float> getBbox(int stride, std::vector<float> anchor) override {
		std::vector<float> ltrb = dfl(this->boxPtr, this->bbox_info_channel);
		float x1 = this->_w + 0.5 - ltrb[0];
		float y1 = this->_h + 0.5 - ltrb[1];
		float x2 = this->_w + 0.5 + ltrb[2];
		float y2 = this->_h + 0.5 + ltrb[3];

		float x = ((x2 + x1) / 2.f) * stride;
		float y = ((y2 + y1) / 2.f) * stride;
		float w = (x2 - x1) * stride;
		float h = (y2 - y1) * stride;

		float* kpt_ptr_start = this->classPtr;
		std::vector<std::vector<float>> one_obj_kpt;
		for (int k = 0; k < this->NOP; ++k) {
			std::vector<float> one_kpt;
			float ori_x = kpt_ptr_start[k * 3];
			float ori_y = kpt_ptr_start[k * 3 + 1];
			float kpt_score = kpt_ptr_start[k * 3 + 2];
			float kpt_x = (ori_x * 2. + this->_w) * stride;
			float kpt_y = (ori_y * 2. + this->_h) * stride;
			float kpt_prob = sigmoid(kpt_score);
			one_kpt = { kpt_x, kpt_y, kpt_prob };
			one_obj_kpt.emplace_back(one_kpt);
		}

		this->kpvec.emplace_back(one_obj_kpt);

		std::vector<float> xywh = { x,y,w,h };
		return xywh;
	}

	

	void nms() {
		auto bbox_num = this->id_list.size();
		for (int i = 0; i < bbox_num; i++)
		{
			this->pose_filter_res.push_back({ this->id_list[i] ,this->socre_list[i],this->box_list[i],this->kpvec[i] });

		}

		std::stable_sort(this->pose_filter_res.begin(), this->pose_filter_res.end(),
			[](const YOLO_POSE_RES& tuple1, const YOLO_POSE_RES& tuple2) {
				return std::get<1>(tuple1) > std::get<1>(tuple2);
			}
		);

		for (auto res : this->pose_filter_res) {
			bool keep = true;
			for (int k = 0; k < this->pose_nms_res.size() && keep; ++k) {
				if (std::get<0>(res) == std::get<0>(this->pose_nms_res[k])) {
					if (1.f - jaccardDistance(std::get<2>(res), std::get<2>(this->pose_nms_res[k])) > this->IOU) {
						keep = false;
					}
				}
			}
			if (keep == true)
				this->pose_nms_res.push_back(res);
		}

	}

	void coordTrans(PicPre& img, bool check_border = true) {
		int left_pad = img.getPad().first;
		int top_pad = img.getPad().second;
		float ratio = img.getRatio().first;
		for (auto&& res : this->pose_nms_res) {
			float class_id = std::get<0>(res);
			float score = std::get<1>(res);
			auto box = std::get<2>(res);
			float x1 = (box.tl().x - left_pad) / ratio;
			float y1 = (box.tl().y - top_pad) / ratio;
			float x2 = (box.br().x - left_pad) / ratio;
			float y2 = (box.br().y - top_pad) / ratio;
			if (check_border) {
				x1 = checkBorder(x1, 0.f, (float)img.src_img.cols);
				y1 = checkBorder(y1, 0.f, (float)img.src_img.rows);
				x2 = checkBorder(x2, 0.f, (float)img.src_img.cols);
				y2 = checkBorder(y2, 0.f, (float)img.src_img.rows);
			}
			float w = x2 - x1;
			float h = y2 - y1;
			//bbox：左上角点和wh
			std::vector<float> one_obj_res;
			one_obj_res = { class_id, x1, y1, w, h, score };

			for (auto i : std::get<3>(res)) {
				auto kpx = (i[0] - left_pad) / ratio;
				auto kpy = (i[1] - top_pad) / ratio;
				auto kpprob = i[2];
				one_obj_res.push_back(kpx);
				one_obj_res.push_back(kpy);
				one_obj_res.push_back(kpprob);
			}

			this->output_data.emplace_back(one_obj_res);
		}
	}



	void visualize(const cv::Mat& img, const std::vector<std::string>& names) {
		std::default_random_engine e;
		std::uniform_int_distribution<unsigned> u(10, 200);
		for (auto res : this->output_data) {
			int class_id = (int)res[0];
			float x1 = res[1];
			float y1 = res[2];
			float w = res[3];
			float h = res[4];
			float score = res[5];
			cv::Scalar color_ = cv::Scalar(u(e), u(e), u(e));
			cv::rectangle(img, cv::Rect2f(x1, y1, w, h), color_, 2);
			std::stringstream ss;
			ss << std::fixed << std::setprecision(2) << score;
			std::string s = std::to_string(class_id) + "_" + "obj" + " " + ss.str();
			auto s_size = cv::getTextSize(s, cv::FONT_HERSHEY_DUPLEX, 0.5, 1, 0);
			cv::rectangle(img, cv::Point2f(x1 - 1, y1 - s_size.height - 7), cv::Point2f(x1 + s_size.width, y1 - 2), color_, -1);
			cv::putText(img, s, cv::Point2f(x1, y1 - 2), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(255, 255, 255), 0.2);
		}

		for (auto res : this->output_data) {
			std::vector < std::vector < float >> oneobj_kpt;
			for (size_t i = 5 + this->NOC; i < res.size(); i += 3)
			{
				std::vector < float > one_point;
				one_point = { res[i],res[i + 1], res[i + 2] };
				oneobj_kpt.push_back(one_point);
			}

			int k = 0;
			for (auto one_kpt : oneobj_kpt) {
				if (one_kpt[2] > 0.5) {
					cv::Scalar color = cv::Scalar(kColorMap[kKptColorIndex[k]][0], kColorMap[kKptColorIndex[k]][1], kColorMap[kKptColorIndex[k]][2]);
					cv::circle(img, { (int)one_kpt[0], (int)one_kpt[1] }, 4, color, -1);
					k++;
				}
			}

			int ske = 0;
			for (auto sk_pair : kCocoSkeleton) {
				int kp1 = sk_pair[0] - 1;
				int kp2 = sk_pair[1] - 1;
				if (((oneobj_kpt[kp1][2]) > 0.5) && ((oneobj_kpt[kp2][2]) > 0.5)) {
					cv::Scalar color = cv::Scalar(kColorMap[kLimbColorIndex[ske]][0], kColorMap[kLimbColorIndex[ske]][1], kColorMap[kLimbColorIndex[ske]][2]);
					cv::line(img,
						{ int(oneobj_kpt[kp1][0]), int(oneobj_kpt[kp1][1]) },
						{ int(oneobj_kpt[kp2][0]), int(oneobj_kpt[kp2][1]) },
						color, 2);
				}
				ske++;

			}
		}
		cv::imshow("results", img);
		cv::waitKey(0);
	}

protected:
	int NOP = 17;        // number of point
	std::vector<std::vector<std::vector<float>>> kpvec;
	using YOLO_POSE_RES = std::tuple<int, float, cv::Rect2f, std::vector<std::vector<float>>>;
	std::vector<YOLO_POSE_RES> pose_filter_res, pose_nms_res;

	int bbox_info_channel;
	virtual void set_ori_out_channles() {
		this->bbox_info_channel = 64;
		this->ori_out_channles = { 1, 64, 51 }; // yolov8-pose 3 heads  class /xywh/key_points
	}
};

REGISTERPANELCLASS(YoloV8_pose_sim);