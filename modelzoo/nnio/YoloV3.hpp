#pragma once
#include "YoloV5.hpp"

class  YoloV3 :public YoloV5 {
public:
	YoloV3() {}
	~YoloV3() {}
	template<typename T>
	std::vector<float> gb(T* tensor_data,
		std::vector<float> norm, int obj_ptr_start, int location_x, int location_y,
		int stride, std::vector<float> anchor) {
		auto xywh = S1::_V3(tensor_data, obj_ptr_start, norm[0]);
		S2::_V3(xywh, location_x, location_y, stride, anchor);
		return xywh;
	}
	virtual std::vector<float> getBbox(int8_t* tensor_data,
		std::vector<float> norm, int obj_ptr_start, int location_x, int location_y,
		int stride, std::vector<float> anchor) {
		auto xywh = (this->bits == 16) ? this->gb((int16_t*)tensor_data, norm
			, obj_ptr_start, location_x, location_y, stride, anchor) : this->gb(tensor_data, norm
				, obj_ptr_start, location_x, location_y, stride, anchor);
		return xywh;
	}
};

REGISTERPANELCLASS(YoloV3);