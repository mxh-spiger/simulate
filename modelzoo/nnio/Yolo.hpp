#pragma once
#include <vector>
#include "opencv2/opencv.hpp"
#include "PicPre.hpp"
#include "utils.hpp"
#include <regex>
#include <random>
#include "NetInfo.hpp"



class  Yolo{

public:
	Yolo() {}
	virtual ~Yolo() {}
	struct extCfg {
		bool MULTILABLE;
		float CONF;
		float IOU;
		int NOC;
		int NOH;
		std::vector<std::vector<std::vector<float>>> ANCHORS;
	};

	// from configure
	bool MULTILABLE;
	float CONF = -1;
	float IOU = -1;
	int NOC = -1;
	int NOH = -1;
	std::vector<std::vector<std::vector<float>>> ANCHORS;
	
	// calc
	//默认为1，初始化时会根据输入anchor数来求
	int NOA = 1;
	float CONF_arcsigmoid = 0;

	// from json
	std::vector<int> stride;                                                                              
	std::vector<float> normalratio;

	
	virtual void nms() {
		auto bbox_num = this->id_list.size();
		for (size_t i = 0; i < bbox_num; i++)
		{
			this->filter_res.push_back({ this->id_list[i],this->socre_list[i],this->box_list[i] });
		}

		std::stable_sort(this->filter_res.begin(), this->filter_res.end(),
			[](const std::tuple<int, float, cv::Rect2f>& tuple1, const std::tuple<int, float, cv::Rect2f>& tuple2) {
				return std::get<1>(tuple1) > std::get<1>(tuple2);
			}
		);

		int idx = 0;
		for (auto res : this->filter_res) {
			bool keep = true;
			for (int k = 0; k < this->nms_res.size() && keep; ++k) {
				if (std::get<0>(res) == std::get<0>(this->nms_res[k])) {
					if (1.f - jaccardDistance(std::get<2>(res), std::get<2>(this->nms_res[k])) > this->IOU) {
						keep = false;
					}
				}

			}
			if (keep == true)
				this->nms_res.emplace_back(res);
			if (idx > this->max_nms) {
				break;
			}
			idx++;
		}
	};

	virtual void coordTrans(PicPre& img, bool check_border = true) {
		int left_pad = img.getPad().first;
		int top_pad = img.getPad().second;
		float ratio = img.getRatio().first;
		//std::cout <<"ratio: " << ratio << std::endl;
		for (auto&& res : this->nms_res) {
			float class_id = std::get<0>(res);
			float score = std::get<1>(res);
			auto box = std::get<2>(res);
			float x1 = (box.tl().x - left_pad) / ratio;
			float y1 = (box.tl().y - top_pad) / ratio;
			float x2 = (box.br().x - left_pad) / ratio;
			float y2 = (box.br().y - top_pad) / ratio;
			if (check_border) {
				x1 = checkBorder(x1, 0.f, (float)img.src_img.cols);
				y1 = checkBorder(y1, 0.f, (float)img.src_img.rows);
				x2 = checkBorder(x2, 0.f, (float)img.src_img.cols);
				y2 = checkBorder(y2, 0.f, (float)img.src_img.rows);
			}
			float w = x2 - x1;
			float h = y2 - y1;
			//bbox：左上角点和wh
			this->output_data.emplace_back(std::vector<float>({ class_id, x1, y1, w, h, score }));
		}
	}

	virtual void saveRes(std::string resRoot, std::string name) {
		std::string save_path = resRoot+"\\" + name;
		std::regex reg(R"(\.(\w*)$)");
		save_path = std::regex_replace(save_path, reg, ".txt");
		std::ofstream outputFile(save_path);
		if (!outputFile.is_open()) {
			std::cout << "Create txt file fail." << std::endl;
		}

		for (auto i : this->output_data) {
			for (auto j : i) {
				outputFile << j << " ";
			}
			outputFile << "\n";
		}
		outputFile.close();
	}

	virtual void visualize(const cv::Mat& img, const std::vector<std::string>& names) {
		std::default_random_engine e;
		std::uniform_int_distribution<unsigned> u(10, 200);
		for (auto res : this->output_data) {
			int class_id = (int)res[0];
			float x1 = res[1];
			float y1 = res[2];
			float w = res[3];
			float h = res[4];
			float score = res[5];
			cv::Scalar color_ = cv::Scalar(u(e), u(e), u(e));
			cv::rectangle(img, cv::Rect2f(x1, y1, w, h), color_, 2);
			std::stringstream ss;
			ss << std::fixed << std::setprecision(2) << score;
			std::string s = std::to_string(class_id) + "_" + names[class_id] + " " + ss.str();
			auto s_size = cv::getTextSize(s, cv::FONT_HERSHEY_DUPLEX, 0.5, 1, 0);
			cv::rectangle(img, cv::Point2f(x1 - 1, y1 - s_size.height - 7), cv::Point2f(x1 + s_size.width, y1 - 2), color_, -1);
			cv::putText(img, s, cv::Point2f(x1, y1 - 2), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(255, 255, 255), 0.2);
		}
		cv::imshow("results", img);
		cv::waitKey(0);
	}

	std::vector<std::vector<float>> getOutputData() {
		return this->output_data;
	}

	void setOutputData(std::vector<std::vector<float>>& res) {
		this->output_data = res;
	}

protected:
	int max_nms = 3000;
	std::vector<int> id_list;
	std::vector<float> socre_list;
	std::vector<cv::Rect2f> box_list;
	std::vector<std::tuple<int, float, cv::Rect2f>> filter_res;
	std::vector<std::tuple<int, float, cv::Rect2f>> nms_res;
	std::vector<std::vector<float>> output_data;
	std::vector<int> ori_out_channles = {};

	//known models` bbox info layout in one head
	//std::vector<int> _yolo_c = { this->NOC };
	//std::vector<int> _yolox_c = { 1,4,this->NOC };
	//std::vector<int> _yolov6_c = { this->NOC,4 };
	//std::vector<int> _yolov8_c = { this->NOC,64 };
	//std::vector<int> _centernet_c = { this->NOC,2,2 };
	int parts = -1;
	std::vector<int> _stride;
	std::vector<std::vector<float>> _norm;

	// 设定一个head里bbox各部分info的通道数
	// 目前用户程序这里从by.json无法获取原始输出的dim信息，若有新情况需要重写此函数
	virtual void set_ori_out_channles() {
		//this->ori_out_channles = _yolo_c;
		std::cerr << "warning: please set model channel layout in yolo.hpp!" << std::endl;
		exit(EXIT_FAILURE);
	}
	
	//根据ori-out-channels设定一个head分几个部分
	void set_parts() {
		this->parts = this->ori_out_channles.size();
	}

	//根据每个head数，将原本1维的norm分组
	void set_norm_by_head() {
		for (size_t i = 0; i < this->NOH; i++)
		{
			std::vector<float> _norm_;
			for (size_t j = 0; j < this->parts; j++)
			{
				_norm_.push_back(this->normalratio[i * this->parts + j]);

			}
			this->_norm.push_back(_norm_);
		}
	}

	//根据head数，将stride折叠（netinfo得到的stride数与输出层数相同）
	void set_stride_by_head() {
		for (size_t i = 0; i < this->NOH * this->parts; i += parts) {
			_stride.push_back(this->stride[i]);
		}
	}
};


