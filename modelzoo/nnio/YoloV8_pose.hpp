#pragma once
#include "YoloHard.hpp"
#include "factory.hpp"
#include "configure.hpp"


class  YoloV8_pose :public YoloHard {
public:
	YoloV8_pose() {}
	~YoloV8_pose() {}
	template<typename T>
	float grs(T* tensor_data,
		int obj_ptr_start, std::vector<float> norm, int index) {
		auto _prob_ = sigmoid(tensor_data[obj_ptr_start + index] * norm[0]);
		return _prob_;
	}

	template<typename T>
	std::pair<int, float> gmrs(T* tensor_data, int obj_ptr_start, std::vector<float> norm) {
		T* class_ptr_start = tensor_data + obj_ptr_start;
		T* max_prob_ptr = std::max_element(class_ptr_start, class_ptr_start + this->NOC);
		int max_index = std::distance(class_ptr_start, max_prob_ptr);
		auto _prob_ = sigmoid(*max_prob_ptr * norm[0]);
		return { max_index, _prob_ };
	}

	template<typename T>
	std::vector<float> gb(T* tensor_data,
		std::vector<float> norm, int obj_ptr_start, int location_x, int location_y,
		int stride, std::vector<float> anchor) {

		std::vector<float> ltrb = dfl(tensor_data, 
			norm[1], obj_ptr_start + this->real_out_channles[0],this->bbox_info_channel);
		float x1 = location_x + 0.5 - ltrb[0];
		float y1 = location_y + 0.5 - ltrb[1];
		float x2 = location_x + 0.5 + ltrb[2];
		float y2 = location_y + 0.5 + ltrb[3];

		float x = ((x2 + x1) / 2.f) * stride;
		float y = ((y2 + y1) / 2.f) * stride;
		float w = (x2 - x1) * stride;
		float h = (y2 - y1) * stride;

		std::vector<float> xywh = { x,y,w,h };

		T* kpt_ptr_start = tensor_data + obj_ptr_start + this->real_out_channles[0] + this->real_out_channles[1];
		std::vector<std::vector<float>> one_obj_kpt;
		for (int k = 0; k < this->NOP; ++k) {
			std::vector<float> one_kpt;
			float ori_x = kpt_ptr_start[k * 3] * norm[2];
			float ori_y = kpt_ptr_start[k * 3 + 1] * norm[2];
			float kpt_score = kpt_ptr_start[k * 3 + 2] * norm[2];
			float kpt_x = (ori_x * 2.  + location_x) * stride  ;
			float kpt_y = (ori_y * 2.  + location_y) * stride  ;
			float kpt_prob = sigmoid(kpt_score);
			one_kpt = { kpt_x, kpt_y, kpt_prob };
			one_obj_kpt.emplace_back(one_kpt);
		}


		this->kpvec.emplace_back(one_obj_kpt);
		return xywh;
	}

	void nms() {
		auto bbox_num = this->id_list.size();
		for (int i = 0; i < bbox_num; i++)
		{
			this->pose_filter_res.push_back({ this->id_list[i] ,this->socre_list[i],this->box_list[i],this->kpvec[i] });

		}

		std::stable_sort(this->pose_filter_res.begin(), this->pose_filter_res.end(),
			[](const YOLO_POSE_RES& tuple1, const YOLO_POSE_RES& tuple2) {
				return std::get<1>(tuple1) > std::get<1>(tuple2);
			}
		);

		for (auto res : this->pose_filter_res) {
			bool keep = true;
			for (int k = 0; k < this->pose_nms_res.size() && keep; ++k) {
				if (std::get<0>(res) == std::get<0>(this->pose_nms_res[k])) {
					if (1.f - jaccardDistance(std::get<2>(res), std::get<2>(this->pose_nms_res[k])) > this->IOU) {
						keep = false;
					}
				}
			}
			if (keep == true)
				this->pose_nms_res.push_back(res);
		}

	}


	float getRealScore(int8_t* tensor_data,
		int obj_ptr_start, std::vector<float> norm, int index) {

		auto s = (this->bits == 16) ? this->grs((int16_t*)tensor_data, obj_ptr_start
			, norm, index) : this->grs(tensor_data, obj_ptr_start, norm, index);

		return s;
	}


	std::pair<int, float> getMaxRealScore(int8_t* tensor_data, int obj_ptr_start, std::vector<float> norm) {

		auto s = (this->bits == 16) ? this->gmrs((int16_t*)tensor_data, obj_ptr_start
			, norm) : this->gmrs(tensor_data, obj_ptr_start, norm);

		return s;
	}


	std::vector<float> getBbox(int8_t* tensor_data,
		std::vector<float> norm, int obj_ptr_start, int location_x, int location_y,
		int stride, std::vector<float> anchor) {
		auto xywh = (this->bits == 16) ? this->gb((int16_t*)tensor_data, norm
			, obj_ptr_start, location_x, location_y, stride, anchor) : this->gb(tensor_data, norm
				, obj_ptr_start, location_x, location_y, stride, anchor);
		return xywh;
	}

	void coordTrans(PicPre& img, bool check_border = true) {
		int left_pad = img.getPad().first;
		int top_pad = img.getPad().second;
		float ratio = img.getRatio().first;
		for (auto&& res : this->pose_nms_res) {
			float class_id = std::get<0>(res);
			float score = std::get<1>(res);
			auto box = std::get<2>(res);
			float x1 = (box.tl().x - left_pad) / ratio;
			float y1 = (box.tl().y - top_pad) / ratio;
			float x2 = (box.br().x - left_pad) / ratio;
			float y2 = (box.br().y - top_pad) / ratio;
			if (check_border) {
				x1 = checkBorder(x1, 0.f, (float)img.src_img.cols);
				y1 = checkBorder(y1, 0.f, (float)img.src_img.rows);
				x2 = checkBorder(x2, 0.f, (float)img.src_img.cols);
				y2 = checkBorder(y2, 0.f, (float)img.src_img.rows);
			}
			float w = x2 - x1;
			float h = y2 - y1;
			//bbox�����Ͻǵ��wh
			std::vector<float> one_obj_res;
			one_obj_res = { class_id, x1, y1, w, h, score };

			for (auto i : std::get<3>(res)) {
				auto kpx = (i[0] - left_pad) / ratio;
				auto kpy = (i[1] - top_pad) / ratio;
				auto kpprob = i[2];
				one_obj_res.push_back(kpx);
				one_obj_res.push_back(kpy);
				one_obj_res.push_back(kpprob);
			}

			this->output_data.emplace_back(one_obj_res);
		}
	}



	void visualize(const cv::Mat& img, const std::vector<std::string>& names) {
		std::default_random_engine e;
		std::uniform_int_distribution<unsigned> u(10, 200);
		for (auto res : this->output_data) {
			int class_id = (int)res[0];
			float x1 = res[1];
			float y1 = res[2];
			float w = res[3];
			float h = res[4];
			float score = res[5];
			cv::Scalar color_ = cv::Scalar(u(e), u(e), u(e));
			cv::rectangle(img, cv::Rect2f(x1, y1, w, h), color_, 2);
			std::stringstream ss;
			ss << std::fixed << std::setprecision(2) << score;
			std::string s = std::to_string(class_id) + "_" + "obj" + " " + ss.str();
			auto s_size = cv::getTextSize(s, cv::FONT_HERSHEY_DUPLEX, 0.5, 1, 0);
			cv::rectangle(img, cv::Point2f(x1 - 1, y1 - s_size.height - 7), cv::Point2f(x1 + s_size.width, y1 - 2), color_, -1);
			cv::putText(img, s, cv::Point2f(x1, y1 - 2), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(255, 255, 255), 0.2);
		}

		for (auto res : this->output_data) {
			std::vector < std::vector < float >> oneobj_kpt;
			for (size_t i = 5 + this->NOC; i < res.size(); i += 3)
			{
				std::vector < float > one_point;
				one_point = { res[i],res[i + 1], res[i + 2] };
				oneobj_kpt.push_back(one_point);
			}

			int k = 0;
			for (auto one_kpt : oneobj_kpt) {
				if (one_kpt[2] > 0.5) {
					cv::Scalar color = cv::Scalar(kColorMap[kKptColorIndex[k]][0], kColorMap[kKptColorIndex[k]][1], kColorMap[kKptColorIndex[k]][2]);
					cv::circle(img, { (int)one_kpt[0], (int)one_kpt[1] }, 4, color, -1);
					k++;
				}
			}

			int ske = 0;
			for (auto sk_pair : kCocoSkeleton) {
				int kp1 = sk_pair[0] - 1;
				int kp2 = sk_pair[1] - 1;
				if (((oneobj_kpt[kp1][2]) > 0.5) && ((oneobj_kpt[kp2][2]) > 0.5)) {
					cv::Scalar color = cv::Scalar(kColorMap[kLimbColorIndex[ske]][0], kColorMap[kLimbColorIndex[ske]][1], kColorMap[kLimbColorIndex[ske]][2]);
					cv::line(img,
						{ int(oneobj_kpt[kp1][0]), int(oneobj_kpt[kp1][1]) },
						{ int(oneobj_kpt[kp2][0]), int(oneobj_kpt[kp2][1]) },
						color, 2);
				}
				ske++;

			}
		}
		cv::imshow("results", img);
		cv::waitKey(0);
	}

protected:
	int NOP = 17 ;        // number of point
	std::vector<std::vector<std::vector<float>>> kpvec;
	using YOLO_POSE_RES = std::tuple<int, float, cv::Rect2f, std::vector<std::vector<float>>>;
	std::vector<YOLO_POSE_RES> pose_filter_res, pose_nms_res;

	int bbox_info_channel;
	virtual void set_ori_out_channles() {
		this->bbox_info_channel = 64;
		this->ori_out_channles = { 1, 64, 51 }; // yolov8-pose 3 heads  class /xywh/key_points
	}
};

REGISTERPANELCLASS(YoloV8_pose);