#pragma once
#include "YoloV6_sim.hpp"
#include "factory.hpp"


class  YoloV8_sim :public YoloV6_sim {
public:
	YoloV8_sim() {}
	~YoloV8_sim() {}

	virtual std::vector<float> getBbox(int stride, std::vector<float> anchor) override {
		std::vector<float> ltrb = dfl(this->boxPtr,this->box_info_length);
		float x1 = this->_w + 0.5 - ltrb[0];
		float y1 = this->_h + 0.5 - ltrb[1];
		float x2 = this->_w + 0.5 + ltrb[2];
		float y2 = this->_h + 0.5 + ltrb[3];

		float x = ((x2 + x1) / 2.f) * stride;
		float y = ((y2 + y1) / 2.f) * stride;
		float w = (x2 - x1) * stride;
		float h = (y2 - y1) * stride;

		std::vector<float> xywh = { x,y,w,h };
		return xywh;
	}

protected:

	virtual void set_ori_out_channles() override {
		this->box_info_length = 64;
		this->ori_out_channles = { this->NOC,this->box_info_length };
	}

};

REGISTERPANELCLASS(YoloV8_sim);