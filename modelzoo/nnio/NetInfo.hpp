#pragma once
#include "runtime.h"
#include <vector>
#include <string>
#include "opencv2/opencv.hpp"



//-------------------- icraft func ----------------------------//

static void dataFormatH2S(std::vector<std::shared_ptr<icraft::rt::RuntimeTensor>>& result_tensor,
    std::shared_ptr<icraft::dev::IcraftDevice> device, std::vector<icraft::rt::RuntimeTensor::ConverterInfo>& infos) {
    int index = 0;
    for (auto i : infos) {
        result_tensor[index] = result_tensor[index]->toHost()->fmHwToSw(i, device);
        index++;
    }
}


class  NetInfo {
public:
    int bits;
    std::vector<int> stride;
    std::vector<float> normalratio;
    std::vector<int> inputshape;  //<n,h,w,c> == <1,h,w,c>
    std::pair<int, int> input_hw;
    std::vector<icraft::rt::RuntimeTensor::ConverterInfo> infos;
    std::vector<std::string> hardOpNames = {};
    bool imk;
    bool icorepost;
    int inputsize = 1;
    int REGBASE = 0x100C0000;

    NetInfo(std::shared_ptr<icraft::rt::RuntimeNetwork> network_ptr) {
        this->inputshape = this->getInputShape(network_ptr);
        this->input_hw = { this->inputshape[1],this->inputshape[2] };
        for (auto i : this->inputshape) {
            inputsize *= i;
        }
        this->hardOpNames = this->getHardOpName(network_ptr);
        this->imk = std::find(this->hardOpNames.begin(), this->hardOpNames.end(), "ImageMake") != this->hardOpNames.end();
        this->icorepost = std::find(this->hardOpNames.begin(), this->hardOpNames.end(), "IcorePost") != this->hardOpNames.end();
        if (!icorepost) {
            this->infoFromOutput(network_ptr);
        }
        else {
            this->infoFromCustomop(network_ptr);
        }
    }

    void infoFromOutput(const std::shared_ptr<icraft::rt::RuntimeNetwork> network_ptr) {
        auto ops = network_ptr->getOps();
        auto output = std::dynamic_pointer_cast<icraft::ir::Operation>(ops[ops.size() - 1]);
        auto inputs_num = output->inputFtmp().size();
        auto inputs = output->inputFtmp();
        for (auto input : inputs) {
            auto scale = input->scale();
            auto norm_ratio = input->normratio();
            auto bits = input->typeBits();
            auto dims = input->dim();
            auto c = input->channDistribution();
            std::vector<int> _dims;
            if (dims.size() == 3) {
                _dims = { 1,c[0].second };
            }
            else {
                _dims = { dims[0],dims[2],dims[3],c[0].second }; //这里认为输出无效通道是在有效通道后的情况，没考虑间隔排布
                int _stride = this->inputshape[1] / dims[2];
                this->stride.push_back(_stride);
            }
            icraft::rt::RuntimeTensor::ConverterInfo info(_dims, scale, norm_ratio, bits);
            this->infos.push_back(info);

        }
    }

    void infoFromCustomop(const std::shared_ptr<icraft::rt::RuntimeNetwork> network_ptr) {
        auto ops = network_ptr->getOps();
        auto customop = std::dynamic_pointer_cast<icraft::ir::Operation>(ops[ops.size() - 2]);
        auto num_c = customop->inputFtmp().size();
        auto output = std::dynamic_pointer_cast<icraft::ir::Operation>(ops[ops.size() - 1]);
        auto num_o = output->inputFtmp().size();
        // 这里逻辑不全面
        // yolox这种分离式的output算子里norm只有3个，不全面因此要从customop中取
        // 但是yoloseg这种，有些不做output的需要去output里取信息
        auto op = (num_c > num_o) ? customop : output;
        auto inputs = op->inputFtmp();
        this->bits = inputs[0]->typeBits();
        for (auto input : inputs) {
            auto norm_ratio = input->normratio()[0];
            this->normalratio.push_back(norm_ratio);
            auto dims = input->dim();
            auto s = (dims.size() == 4) ? (this->inputshape[1] / dims[1]) : (this->inputshape[1] / dims[2]);
            this->stride.push_back(s);
        }

    }

    std::vector<std::string> getHardOpName(const std::shared_ptr<icraft::rt::RuntimeNetwork> network_ptr) {
        std::vector<std::string> hardOpNames = {};
        auto dllmap = network_ptr->dlls();
        for (auto iter = dllmap.begin(); iter != dllmap.end(); iter++) {
            std::string dllname = iter->first;
            hardOpNames.push_back(dllname);
        }

        return hardOpNames;
    }

    // icraft2.1升级了layout的判别方式导致次函数取inputshape方法发生变化；当layout为normallayout则输入按照源尺寸读入，这里默认为4维输入的图片<n,h,w,c>
    // return net inputs shape  <1,h,w,c>
    std::vector<int> getInputShape(const std::shared_ptr<icraft::rt::RuntimeNetwork> network_ptr) {
        auto ops = network_ptr->getOps();
        auto input = std::dynamic_pointer_cast<icraft::ir::Operation>(ops[0]);
        auto inputImg = input->outputFtmp();
        int h, w, channel;
        auto layout = inputImg[0]->layoutName();
        if (layout == "NormalLayout") {
            channel = inputImg[0]->dim()[3];
            h = inputImg[0]->dim()[1];
            w = inputImg[0]->dim()[2];
        }
        else {
            channel = inputImg[0]->channDistribution()[0].second;
            h = inputImg[0]->dim()[2];
            w = inputImg[0]->dim()[3];
        }
        return { 1, h, w ,channel };
    }

    // cv mat to icraft runtime tensor
    std::shared_ptr<icraft::rt::RuntimeTensor> cvMat2Tensor(const cv::Mat& m) {
        std::shared_ptr<uint8_t[]> data(new uint8_t[this->inputsize]);
        std::copy_n((uint8_t*)m.data, this->inputsize, data.get());
        auto cv_tensor = std::make_shared<icraft::rt::RuntimeTensor>(data, this->inputshape);
        return cv_tensor;
    }

    //init dma device
    void dmaInit(const std::shared_ptr<icraft::dev::IcraftDevice> device,
        const std::shared_ptr<icraft::rt::RuntimeTensor> img_tensor,
        // using u-dma-buf(cma,with cache) in linux;(reserved physical memory in other way is also ok)
        const std::shared_ptr<icraft::dev::MemChunk> reserve_mem_chunck
    ) {
        //init DMA device
        auto ImageMakeRddrBase = reserve_mem_chunck->physAddr();
        auto ImageMakeHeight = this->inputshape[1];
        auto ImageMakeWidth = this->inputshape[2];
        auto ImageMakeChannel = this->inputshape[3];
        auto ImageMakeRlen = ((ImageMakeWidth * ImageMakeHeight - 1) / (24 / ImageMakeChannel) + 1) * 3;   //this 
        auto ImageMakeLastSft = ImageMakeWidth * ImageMakeHeight - (ImageMakeRlen - 3) / 3 * (24 / ImageMakeChannel);
        device->writeReg(this->REGBASE + 0x4, ImageMakeRddrBase, true);
        device->writeReg(this->REGBASE + 0x8, ImageMakeRlen, true);
        device->writeReg(this->REGBASE + 0xC, ImageMakeLastSft, true);
        device->writeReg(this->REGBASE + 0x10, ImageMakeChannel, true);
        device->writeReg(this->REGBASE + 0x1C, 1, true); // 1 -> data from hp
        device->writeReg(this->REGBASE + 0x20, 0, true);

        auto tensor_data = img_tensor->data<uint8_t>(); //图片原始密排数据
        try {
            icraft::dev::ChunkManager::memcpy(reserve_mem_chunck, tensor_data.get(), this->inputsize);
        }
        catch (std::exception& e) {
            std::cout << e.what() << std::endl;
        }
    }

    void data_cover_test(const std::shared_ptr<icraft::dev::IcraftDevice> device,
        const std::shared_ptr<icraft::rt::RuntimeTensor> img_tensor,
        // using u-dma-buf(cma,with cache) in linux;(reserved physical memory in other way is also ok)
        const std::shared_ptr<icraft::dev::MemChunk> reserve_mem_chunck
    ) {
        auto tensor_data = img_tensor->data<uint8_t>(); //图片原始密排数据
        try {
            icraft::dev::ChunkManager::memcpy(reserve_mem_chunck, tensor_data.get(), this->inputsize);
        }
        catch (std::exception& e) {
            std::cout << e.what() << std::endl;
        }
    }

    void dmaStart(const std::shared_ptr<icraft::dev::IcraftDevice> device) {
        device->writeReg(this->REGBASE, 1, true);
    }

    std::shared_ptr<icraft::dev::MemChunk> requestMem(const std::shared_ptr<icraft::dev::IcraftDevice> device) {
        return device->getMM(icraft::dev::UDMABUF)->mallocMem(this->inputsize);
    }

    std::shared_ptr<icraft::rt::RuntimeTensor> maketensor(std::shared_ptr<icraft::dev::IcraftDevice> device, const cv::Mat& m) {
        auto cv_tensor = this->cvMat2Tensor(m);
        auto chunk_udma = device->getMM(icraft::dev::UDMABUF)->mallocMem(this->inputsize);
        if (this->imk) {
            this->dmaInit(device, cv_tensor, chunk_udma);
            this->dmaStart(device);
        }
        chunk_udma->free();
        return cv_tensor;
    }

};
