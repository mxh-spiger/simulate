#pragma once
#include <vector>
#include "Yolo.hpp"
#include "opencv2/opencv.hpp"
#include "runtime.h"
#include "NetInfo.hpp"
#include "bbox_formula.hpp"
#include<bitset>
class  YoloHard :public Yolo {
public:
	YoloHard() {}
	virtual ~YoloHard() {}
	//after hard filter
	void filter(const std::vector<std::shared_ptr<icraft::rt::RuntimeTensor>>& results) {
		this->icraft_results = results;
		for (int head_index = 0; head_index < this->NOH; head_index++) {
			int output_size = (results[head_index]->totalBytes()) / (this->bits / 8);
			if ((output_size % this->anchor_length)) {
				std::cout << "output_size: " << output_size << std::endl;
				std::cout << "anchor_length: " << this->anchor_length << std::endl;
				throw "output_size % anchor_length is not 0";
				exit(EXIT_FAILURE);
			}
			int objnum = output_size / this->anchor_length;
			auto norm = this->_norm[head_index];
			auto stride = this->_stride[head_index];
			std::vector<float> _anchor_ = {};
			switch (this->bits) {
			case 8: {
				int8_t* tensor_data = results[head_index]->data<int8_t>().get();
				for (int i = 0; i < objnum; i++) {
					auto obj_ptr_start = i * this->anchor_length;
					auto obj_ptr_next = obj_ptr_start + this->anchor_length;
					//uint16_t anchor_index = ((((std::bitset<16>)tensor_data[obj_ptr_next - 1]) << 8) | (((std::bitset<16>)tensor_data[obj_ptr_next - 2]) & (std::bitset<16>)0x00ff )).to_ullong();
					//uint16_t location_y = ((((std::bitset<16>)tensor_data[obj_ptr_next - 3]) << 8) | (((std::bitset<16>)tensor_data[obj_ptr_next - 4]) & (std::bitset<16>)0x00ff)).to_ullong();
					//uint16_t location_x = ((((std::bitset<16>)tensor_data[obj_ptr_next - 5]) << 8) | (((std::bitset<16>)tensor_data[obj_ptr_next - 6]) & (std::bitset<16>)0x00ff)).to_ullong();
					
					//低8位不能转成uint16_t,虽然是无符号数，但高位还是会补符号
					uint16_t anchor_index = (((uint16_t)tensor_data[obj_ptr_next - 1]) << 8) + (uint8_t)tensor_data[obj_ptr_next - 2];
					uint16_t location_y   = (((uint16_t)tensor_data[obj_ptr_next - 3]) << 8) + (uint8_t)tensor_data[obj_ptr_next - 4];
					uint16_t location_x   = (((uint16_t)tensor_data[obj_ptr_next - 5]) << 8) + (uint8_t)tensor_data[obj_ptr_next - 6];

					if (this->ANCHORS.size()!=0) {
						_anchor_ = this->ANCHORS[head_index][anchor_index];
					}
					this->oneObj(tensor_data, obj_ptr_start, location_x, location_y, norm, stride, _anchor_);
				}
				break;
			}
			case 16: {
				int16_t* tensor_data = results[head_index]->data<int16_t>().get();
				for (int i = 0; i < objnum; i++) {
					int obj_ptr_start = i * this->anchor_length;
					int obj_ptr_next = obj_ptr_start + this->anchor_length;
					uint16_t anchor_index = tensor_data[obj_ptr_next - 1];
					uint16_t location_y = tensor_data[obj_ptr_next - 2];
					uint16_t location_x = tensor_data[obj_ptr_next - 3];
					if (this->ANCHORS.size() != 0) {
						_anchor_ = this->ANCHORS[head_index][anchor_index];
					}
					this->oneObj((int8_t*)tensor_data, obj_ptr_start, location_x, location_y, norm, stride, _anchor_);
				}
				break;
			}
			default: {
				throw "wrong bits num!";
				exit(EXIT_FAILURE);
			}
			}
			
		}
	}

	void oneObj(int8_t* tensor_data, int obj_ptr_start,
		int location_x, int location_y, std::vector<float> norm, int stride, 
		std::vector<float> anchor) {

		if (!this->MULTILABLE) {
			auto i_s = this->getMaxRealScore(tensor_data, obj_ptr_start, norm);
			auto max_index = i_s.first;
			auto realscore = i_s.second;
			if (realscore > this->CONF) {
				auto bbox = this->getBbox(tensor_data, norm, obj_ptr_start, location_x, location_y, stride, anchor);
				this->id_list.emplace_back(max_index);
				this->socre_list.emplace_back(realscore);
				this->box_list.emplace_back(cv::Rect2f((bbox[0] - bbox[2] / 2),
					(bbox[1] - bbox[3] / 2), bbox[2], bbox[3]));	
			}
		}
		else {
			for (size_t i = 0; i < this->NOC; i++) {
				auto realscore = this->getRealScore(tensor_data, obj_ptr_start, norm,i);
				if (realscore > this->CONF) {
					auto bbox = this->getBbox(tensor_data, norm, obj_ptr_start, location_x, location_y, stride, anchor);
					this->id_list.emplace_back(i);
					this->socre_list.emplace_back(realscore);
					this->box_list.emplace_back(cv::Rect2f((bbox[0] - bbox[2] / 2),
						(bbox[1] - bbox[3] / 2), bbox[2], bbox[3]));
				}
			}
		}
	}
	
	virtual void init(extCfg& cfg, NetInfo& netinfo) {
		this->CONF = cfg.CONF;
		this->IOU = cfg.IOU;
		this->NOC = cfg.NOC;
		this->NOH = cfg.NOH;
		this->ANCHORS = cfg.ANCHORS;
		this->MULTILABLE = cfg.MULTILABLE;
		if (cfg.ANCHORS.size()) {
			this->NOA = this->ANCHORS[0].size();
		}
		this->CONF_arcsigmoid = arcSigmoid(this->CONF);
		this->stride = netinfo.stride;
		this->normalratio = netinfo.normalratio;
		this->set_ori_out_channles();
		this->set_parts();
		this->set_norm_by_head();
		this->set_stride_by_head();
		
		//hard filter need
		this->bits = netinfo.bits;
		this->_setc();
		this->_getAnchorLength();
		
		

	}
	
	//init print
	virtual void init_debug() {
		std::cout << "iou: " << this->IOU<<std::endl;
		for (auto i : this->normalratio)
		{
			std::cout << "norm: " << i << std::endl;
		}

		for (auto i : this->ori_out_channles)
		{
			std::cout << "ori o c: " << i << std::endl;
		}
		for (auto i : this->_norm)
		{
			for (auto j : i)
			{
				std::cout << "intri norm: " << j << std::endl;
			}
		}
		for (auto i : this->_stride)
		{
			std::cout << "stride: " << i << std::endl;
		}
		for (auto i : this->real_out_channles)
		{
			std::cout << "real oc: " << i << std::endl;
		}
		std::cout << "anchorl: " << this->anchor_length << std::endl;
		std::cout << "parts: " << this->parts << std::endl;
		std::cout << "noc: " << this->NOC << std::endl;
	};
	
	virtual float getRealScore(int8_t* tensor_data, int obj_ptr_start, std::vector<float> norm, int index) = 0;

	
	virtual std::pair<int, float> getMaxRealScore(int8_t* tensor_data, int obj_ptr_start, std::vector<float> norm) = 0;

	
	virtual std::vector<float> getBbox(int8_t* tensor_data, std::vector<float> norm, int obj_ptr_start, int location_x,
		int location_y, int stride, std::vector<float> anchor) = 0;

protected:
	int bits = -1;
	int maxc = -1;
	int minc = -1;
	int anchor_length = -1;
	std::vector<int> real_out_channles = {};
	std::vector<std::shared_ptr<icraft::rt::RuntimeTensor>> icraft_results;

	void _setc() {
		switch (this->bits)
		{
		case 8: {
			this->maxc = 64;
			this->minc = 8;
			break;
		}
		case 16: {
			this->maxc = 32;
			this->minc = 4;
			break;
		}
		default:
			//浮点的时候有15bit，所以这里直接break
			/*throw "wrong bits num, only support 8 & 16!";
			exit(EXIT_FAILURE);*/
			break;
		}
	}
	
	//calc for anchor length. the last part should be supplemented with the integral multiple of min channel
	int _last_c(int ori_c) {
		return ceil( (float)ori_c / (float)this->minc) * this->minc + this->minc;
	}

	//calc for anchor length. the !last part should be supplemented with the integral multiple of max channel
	int _mid_c(int ori_c) {
		return ceil((float)ori_c / (float)this->maxc) * this->maxc;
	}
	
	//根据输出通道数（ori_out_channles）计算anchorlength，情况有限，可能需要重写
	virtual void  _getAnchorLength() {
		switch (this->parts) {
		case 1: {
			auto oneAnchor = this->ori_out_channles[0] / this->NOA;
			this->anchor_length = _last_c((float)oneAnchor);
			this->real_out_channles = { this->NOA * this->anchor_length };
			break;
		}
		case 2: {
			this->anchor_length = _last_c((float)this->ori_out_channles[1])
				+ _mid_c(this->ori_out_channles[0]);
			this->real_out_channles = { _mid_c(this->ori_out_channles[0]), _last_c((float)this->ori_out_channles[1]) };
			break;
		}
		case 3: {
			this->anchor_length = _last_c((float)this->ori_out_channles[2])
				+ _mid_c(this->ori_out_channles[1])+ _mid_c(this->ori_out_channles[0]);
			this->real_out_channles = { _mid_c(this->ori_out_channles[0]) ,_mid_c(this->ori_out_channles[1]) ,
			_last_c((float)this->ori_out_channles[2]) };
			break;
		}
		default: {
			throw "parts > 3, icorepost支持1个bbox的信息最多分散在3层中!";
			exit(EXIT_FAILURE);
		}

		}
	}

};

