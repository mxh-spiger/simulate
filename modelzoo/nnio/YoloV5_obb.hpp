#pragma once
#include "YoloV5.hpp"

class  YoloV5_obb :public YoloV5 {
public:
	YoloV5_obb() {}
	~YoloV5_obb() {}
	template<typename T>
	std::vector<float> gb(T* tensor_data,
		std::vector<float> norm, int obj_ptr_start, int location_x, int location_y,
		int stride, std::vector<float> anchor) {
		auto xywh = S1::_V5(tensor_data, obj_ptr_start, norm[0]);
		S2::_V5(xywh, location_x, location_y, stride, anchor);
		T* angle_ptr_start = tensor_data + obj_ptr_start + 5 + this->NOC;
		T* max_prob_ptr = std::max_element(angle_ptr_start, angle_ptr_start + 180);
		int max_index = std::distance(angle_ptr_start, max_prob_ptr);
		this->angle_list.push_back(max_index);
		return xywh;
	}

	virtual std::vector<float> getBbox(int8_t* tensor_data,
		std::vector<float> norm, int obj_ptr_start, int location_x, int location_y,
		int stride, std::vector<float> anchor) {
		auto xywh = (this->bits == 16) ? this->gb((int16_t*)tensor_data, norm
			, obj_ptr_start, location_x, location_y, stride, anchor) : this->gb(tensor_data, norm
				, obj_ptr_start, location_x, location_y, stride, anchor);
		return xywh;
	}
	
	void nms() {
		
		auto bbox_num = this->id_list.size();
		for (size_t i = 0; i < bbox_num; i++)
		{
			this->obb_filter_res.push_back({ this->id_list[i],this->socre_list[i],this->box_list[i],this->angle_list[i] });
		}

		std::stable_sort(this->obb_filter_res.begin(), this->obb_filter_res.end(),
			[](const YOLO_OBB_RES& tuple1, const YOLO_OBB_RES& tuple2) {
				return std::get<1>(tuple1) > std::get<1>(tuple2);
			}
		);
		int idx = 0;
		for (auto res : this->obb_filter_res) {
			bool keep = true;
			for (int k = 0; k < this->obb_nms_res.size() && keep; ++k) {
				if (std::get<0>(res) == std::get<0>(this->obb_nms_res[k])) {
					if (1.f - jaccardDistance(std::get<2>(res), std::get<2>(this->obb_nms_res[k])) > this->IOU) {
						keep = false;
					}
				}

			}
			if (keep == true)
				this->obb_nms_res.emplace_back(res);
			if (idx > this->max_nms) {
				break;
			}
			idx++;
		}
}

	void coordTrans(PicPre& img, bool check_border = true) {
		int left_pad = img.getPad().first;
		int top_pad = img.getPad().second;
		float ratio = img.getRatio().first;
		for (auto&& res : this->obb_nms_res) {
			float class_id = std::get<0>(res);
			float score = std::get<1>(res);
			auto box = std::get<2>(res);
			float x1 = (box.tl().x - left_pad) / ratio;
			float y1 = (box.tl().y - top_pad) / ratio;
			float x2 = (box.br().x - left_pad) / ratio;
			float y2 = (box.br().y - top_pad) / ratio;
			if (check_border) {
				x1 = checkBorder(x1, 0.f, (float)img.src_img.cols);
				y1 = checkBorder(y1, 0.f, (float)img.src_img.rows);
				x2 = checkBorder(x2, 0.f, (float)img.src_img.cols);
				y2 = checkBorder(y2, 0.f, (float)img.src_img.rows);
			}
			float w = x2 - x1;
			float h = y2 - y1;

			float theta_pred_index = std::get<3>(res) - 90;
			float theta_pred = theta_pred_index / 180 * 3.141592;

			float x0 = x1 + w / 2;
			float y0 = y1 + h / 2;

			float Cos = cos(theta_pred);
			float Sin = sin(theta_pred);

			float w_cos = w / 2 * Cos;
			float w_sin = -w / 2 * Sin;
			float h_sin = -h / 2 * Sin;
			float h_cos = -h / 2 * Cos;

			float point_x1 = x0 + w_cos + h_sin;
			float point_y1 = y0 + w_sin + h_cos;
			float point_x2 = x0 + w_cos - h_sin;
			float point_y2 = y0 + w_sin - h_cos;
			float point_x3 = x0 - w_cos - h_sin;
			float point_y3 = y0 - w_sin - h_cos;
			float point_x4 = x0 - w_cos + h_sin;
			float point_y4 = y0 - w_sin + h_cos;
			//bbox：左上角点和wh
			this->output_data.emplace_back(std::vector<float>({ class_id, point_x1, point_y1, point_x2, point_y2,
				point_x3,point_y3,point_x4,point_y4,score }));
		}
	}

	void visualize(const cv::Mat& img, const std::vector<std::string>& names) {
		std::default_random_engine e;
		std::uniform_int_distribution<unsigned> u(10, 200);
		for (auto res : this->output_data) {
			int class_id = (int)res[0];
			float score = res[9];
			float point_x1 = res[1];
			float point_y1 = res[2];
			float point_x2 = res[3];
			float point_y2 = res[4];							 
			float point_x3 = res[5];
			float point_y3 = res[6];
			float point_x4 = res[7];
			float point_y4 = res[8];

			std::vector<std::vector<cv::Point>> point_list = { { cv::Point(point_x1,point_y1),cv::Point(point_x2,point_y2),
				cv::Point(point_x3,point_y3),cv::Point(point_x4,point_y4) } };
			cv::Scalar color_ = cv::Scalar(u(e), u(e), u(e));
			cv::drawContours(img, point_list, -1, color_, 2);
			std::stringstream ss;
			ss << std::fixed << std::setprecision(2) << score;
			std::string s = std::to_string(class_id) + "_" + names[class_id] + " " + ss.str();
			auto s_size = cv::getTextSize(s, cv::FONT_HERSHEY_DUPLEX, 0.5, 1, 0);

			cv::rectangle(img, cv::Point2f(point_x1 - 1, point_y1 - s_size.height - 7), cv::Point2f(point_x1 + s_size.width, point_y1 - 2), color_, -1);
			cv::putText(img, s, cv::Point2f(point_x1, point_y1 - 2), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(255, 255, 255), 0.2);
		}
		cv::imshow("results", img);
		cv::waitKey(0);
	}

protected:
	void set_ori_out_channles() {
		this->ori_out_channles = { 3 * (this->NOC + 5 + 180) };//只适用于180选1个角度的obb网络；
	}
	std::vector<int> angle_list = {};
	using YOLO_OBB_RES = std::tuple<int, float, cv::Rect2f, int>;
	std::vector<YOLO_OBB_RES> obb_filter_res, obb_nms_res;
};

REGISTERPANELCLASS(YoloV5_obb);