#pragma once
#include "YoloV5_seg.hpp"
#include "chrono"
using namespace std::chrono;

class  YoloV5_seg_2stage :public YoloV5_seg {
public:
	YoloV5_seg_2stage() {}
	~YoloV5_seg_2stage() {}

	void coordTrans(PicPre& img, std::shared_ptr<icraft::rt::Runtime> runtime_view2, std::shared_ptr<icraft::rt::Runtime> runtime_view3,std::shared_ptr<icraft::dev::IcraftDevice> device) {
		int left_pad = img.getPad().first;
		int top_pad = img.getPad().second;
		float ratio = img.getRatio().first;
		auto proto = this->icraft_results[3];
		icraft::rt::RuntimeTensor::ConverterInfo info({ 1,160,160,1 }, { 1,0 }, 0.006725050508975983, 8);
		auto start = system_clock::now();
		for (auto&& res : this->seg_nms_res) {
			float class_id = std::get<0>(res);
			float score = std::get<1>(res);
			auto box = std::get<2>(res);
			float x1 = (box.tl().x - left_pad) / ratio;
			float y1 = (box.tl().y - top_pad) / ratio;
			float x2 = (box.br().x - left_pad) / ratio;
			float y2 = (box.br().y - top_pad) / ratio;
			x1 = checkBorder(x1, 0.f, (float)img.src_img.cols);
			y1 = checkBorder(y1, 0.f, (float)img.src_img.rows);
			x2 = checkBorder(x2, 0.f, (float)img.src_img.cols);
			y2 = checkBorder(y2, 0.f, (float)img.src_img.rows);
			float w = x2 - x1;
			float h = y2 - y1;
			//bbox�����Ͻǵ��wh
			this->output_data.emplace_back(std::vector<float>({ class_id, x1, y1, w, h, score }));
			std::vector<float> masks_kernel = std::get<3>(res);
			std::vector<float> masks_out;
			std::shared_ptr<float[]> data(new float[32]);
			std::copy_n(masks_kernel.data(), 32, data.get());
			std::vector<int> dims = { 1,1,32,1,1 };
			auto masks_kernel_tensor = std::make_shared<icraft::rt::RuntimeTensor>(data, dims);
			//for (auto i: proto->dims()) {
			//	std::cout << "proto dim: " << i << std::endl;
			//}
			//for (auto i : masks_kernel_tensor->dims()) {
			//	std::cout << "kernel dim: " << i << std::endl;
			//}
			
			//auto mask_res = runtime_view2->forward({ proto ,masks_kernel_tensor });
			auto f1_start = system_clock::now();
			auto reshape_res = runtime_view2->forward({masks_kernel_tensor});
			auto f1_end = system_clock::now();

			reshape_res.insert(reshape_res.begin(), proto);
			for (auto&& i : reshape_res) {
				i->setReady(true);
			}

			auto f_start = system_clock::now();
			auto mask_res = runtime_view3->forward(reshape_res );
			auto f_end = system_clock::now();

			auto trans_start = system_clock::now();
			mask_res[0] = mask_res[0]->toHost()->fmHwToSw(info, device);
			auto trans_end = system_clock::now();
			auto results = mask_res[0]->data<float>().get();
			for (size_t i = 0; i < 160*160; i++)
			{
				masks_out.push_back(results[i]);
			}

			this->allmask.emplace_back(masks_out);
			auto t_duration = duration_cast<std::chrono::milliseconds>(trans_end - trans_start);
			auto f_duration = duration_cast<std::chrono::milliseconds>(f_end - f_start);
			auto f1_duration = duration_cast<std::chrono::milliseconds>(f1_end - f1_start);
			std::cout << "trans time: " << t_duration.count() << std::endl;
			std::cout << "forward1 time: " << f1_duration.count() << std::endl;
			std::cout << "forward2 time: " << f_duration.count() << std::endl;
		}
		auto end = system_clock::now();
		auto duration = duration_cast<std::chrono::milliseconds>(end - start);
		
		std::cout << "time: " << duration.count() << std::endl;
		runtime_view2->reset();
	}
};
REGISTERPANELCLASS(YoloV5_seg_2stage);