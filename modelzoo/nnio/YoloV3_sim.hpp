#pragma once
#include "YoloV5_sim.hpp"

class  YoloV3_sim :public YoloV5_sim {
public:
	YoloV3_sim() {}
	~YoloV3_sim() {}
	virtual std::vector<float> getBbox(int stride, std::vector<float> anchor) override {
		auto xywh = S1::_V3(this->boxPtr);
		S2::_V3(xywh, this->_w, this->_h, stride, anchor);
		return xywh;
	}
};

REGISTERPANELCLASS(YoloV3_sim);