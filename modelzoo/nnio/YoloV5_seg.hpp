#pragma once
#include "YoloV5.hpp"
#include <Eigen/Core>
#include <Eigen/Dense>
#include <opencv2/core/eigen.hpp>
#include <opencv2/opencv.hpp>
#include <chrono>

using namespace std::chrono;

class  YoloV5_seg :public YoloV5 {
public:
	YoloV5_seg() {}
	~YoloV5_seg() {}
	template<typename T>
	std::vector<float> gb(T* tensor_data,
		std::vector<float> norm, int obj_ptr_start, int location_x, int location_y,
		int stride, std::vector<float> anchor) {
		auto xywh = S1::_V5(tensor_data, obj_ptr_start, norm[0]);
		S2::_V5(xywh, location_x, location_y, stride, anchor);
		int seg_ptr_start = obj_ptr_start + 5 + this->NOC;
		std::vector<float> one_obj_mask_info;
		for (size_t i = 0; i < this->mask_channel; i++)
		{
			one_obj_mask_info.push_back(tensor_data[seg_ptr_start + i] * norm[0]);
		}
		this->mask_info.push_back(one_obj_mask_info);
		return xywh;
	}

	virtual std::vector<float> getBbox(int8_t* tensor_data,
		std::vector<float> norm, int obj_ptr_start, int location_x, int location_y,
		int stride, std::vector<float> anchor) {
		auto xywh = (this->bits == 16) ? this->gb((int16_t*)tensor_data, norm
			, obj_ptr_start, location_x, location_y, stride, anchor) : this->gb(tensor_data, norm
				, obj_ptr_start, location_x, location_y, stride, anchor);
		return xywh;
	}

	virtual void nms() {
		auto bbox_num = this->id_list.size();
		for (int i = 0; i < bbox_num; i++)
		{
			this->seg_filter_res.push_back({ this->id_list[i] ,this->socre_list[i],
				this->box_list[i],this->mask_info[i] });
		}

		std::stable_sort(this->seg_filter_res.begin(), this->seg_filter_res.end(),
			[](const YOLO_SEG_RES& tuple1, const YOLO_SEG_RES& tuple2) {
				return std::get<1>(tuple1) > std::get<1>(tuple2);
			}
		);
		int idx = 0;
		for (auto res : this->seg_filter_res) {
			bool keep = true;
			for (int k = 0; k < this->seg_nms_res.size() && keep; ++k) {
				if (std::get<0>(res) == std::get<0>(this->seg_nms_res[k])) {
					if (1.f - jaccardDistance(std::get<2>(res), std::get<2>(this->seg_nms_res[k])) > this->IOU) {
						keep = false;
					}
				}
			}
			if (keep == true)
				this->seg_nms_res.push_back(res);
			if (idx > this->max_nms) {
				break;
			}
			idx++;
		}

	}

	virtual void coordTrans(PicPre& img, bool check_border = true) {
		int left_pad = img.getPad().first;
		int top_pad = img.getPad().second;
		float ratio = img.getRatio().first;
		auto proto = this->icraft_results[3]->toHost();
		Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> mask_proto;
		
		auto start = steady_clock::now();
		auto mask_datasize = this->protoh * this->protow;
		if (this->bits == 8) {
			auto proto_8 = proto->data<int8_t>().get();
			mask_proto = Eigen::Map<Eigen::Matrix<int8_t, Eigen::Dynamic, Eigen::Dynamic>>
				(proto_8, this->mask_channel, mask_datasize).cast<float>();
		}
		else if (this->bits == 16) {
			auto proto_16 = proto->data<int16_t>().get();
			Eigen::TensorMap<Eigen::Tensor<int16_t, 3, Eigen::RowMajor>, Eigen::Aligned> tensor_map(proto_16, 2, 25600, 16);
			Eigen::array<Eigen::Index, 3> permute_dims({ 0, 2, 1 });
			Eigen::Tensor<int16_t, 3, Eigen::RowMajor> transposed = tensor_map.shuffle(permute_dims);
			Eigen::array<Eigen::Index, 2> new_dims({ 25600, 32 });
			Eigen::TensorMap<Eigen::Tensor<int16_t, 2, Eigen::RowMajor>, Eigen::Aligned> reshaped(transposed.data(), new_dims);
			mask_proto = Eigen::Map<Eigen::Matrix<int16_t, Eigen::Dynamic, Eigen::Dynamic>>
				(reshaped.data(), mask_datasize, this->mask_channel).transpose().cast<float>();
		}
		else {
			std::cerr << "yolo_seg coordTrans wrong bits!";
		}
		auto end1 = steady_clock::now();
		
		Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> mask_kernel;
		int objnum = this->seg_nms_res.size();
		mask_kernel.resize(objnum, this->mask_channel);
		int obj_idn = 0;
		for (auto&& res : this->seg_nms_res) {
			float class_id = std::get<0>(res);
			float score = std::get<1>(res);
			auto box = std::get<2>(res);
			float x1 = (box.tl().x - left_pad) / ratio;
			float y1 = (box.tl().y - top_pad) / ratio;
			float x2 = (box.br().x - left_pad) / ratio;
			float y2 = (box.br().y - top_pad) / ratio;
			if (check_border) {
				x1 = checkBorder(x1, 0.f, (float)img.src_img.cols);
				y1 = checkBorder(y1, 0.f, (float)img.src_img.rows);
				x2 = checkBorder(x2, 0.f, (float)img.src_img.cols);
				y2 = checkBorder(y2, 0.f, (float)img.src_img.rows);
			}
			float w = x2 - x1;
			float h = y2 - y1;
			//bbox：左上角点和wh
			this->output_data.emplace_back(std::vector<float>({ class_id, x1, y1, w, h, score }));
			std::vector<float> masks_in = std::get<3>(res);
			Eigen::VectorXf vec(this->mask_channel);
			vec = Eigen::Map<Eigen::VectorXf>(masks_in.data(), this->mask_channel);
			mask_kernel.row(obj_idn) = vec;
			obj_idn++;									
		}
		this->mask_res.resize(objnum, mask_datasize);
		auto end2 = steady_clock::now();
		this->mask_res = mask_kernel * mask_proto;
		auto end3 = steady_clock::now();
		_sigmoid(this->mask_res.data(), mask_datasize * objnum, this->normalratio[3], true);
		auto end4 = steady_clock::now();
		auto end = steady_clock::now();


	}

	virtual void visualize(const cv::Mat& img, const std::vector<std::string>& names) {
		std::default_random_engine e;
		std::uniform_int_distribution<unsigned> u(10, 200);
		int index = 0;
		cv::Mat mask = cv::Mat::zeros(cv::Size(img.cols, img.rows), img.type());
		cv::Mat rgb_mask = cv::Mat::zeros(cv::Size(img.cols, img.rows), img.type());
		auto dst_size = (img.cols >= img.rows) ? img.cols : img.rows;
		auto mask_size = (this->protoh >= this->protoh) ? this->protoh : this->protoh;
		auto ratio = (float)dst_size / (float)mask_size;
		Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> mask_single;
		for (auto res : this->output_data) {
			int class_id = (int)res[0];
			float x1 = res[1];
			float y1 = res[2];
			float w = res[3];
			float h = res[4];
			float score = res[5];
			cv::Scalar color_ = cv::Scalar(u(e), u(e), u(e));
			cv::rectangle(img, cv::Rect2f(x1, y1, w, h), color_, 2);
			std::stringstream ss;
			ss << std::fixed << std::setprecision(2) << score;
			std::string s = std::to_string(class_id) + "_" + names[class_id] + " " + ss.str();
			auto s_size = cv::getTextSize(s, cv::FONT_HERSHEY_DUPLEX, 0.5, 1, 0);
			cv::rectangle(img, cv::Point2f(x1 - 1, y1 - s_size.height - 7), cv::Point2f(x1 + s_size.width, y1 - 2), color_, -1);
			cv::putText(img, s, cv::Point2f(x1, y1 - 2), cv::FONT_HERSHEY_DUPLEX, 0.5, cv::Scalar(255, 255, 255), 0.2);
			//获取每个物体的mask
			auto pic_size = this->protow * this->protoh;
			Eigen::VectorXf v(pic_size);
			v = this->mask_res.row(index);
			mask_single = Eigen::Map<Eigen::MatrixXf>(v.data(), this->protoh, this->protow).transpose();
			cv::Mat masks_1;
			cv::eigen2cv(mask_single, masks_1);
			cv::resize(masks_1, masks_1, cv::Size(this->protow * ratio, this->protoh * ratio));
			// 将masked目标从原图上裁剪下来
			cv::Mat masks_3 = masks_1(cv::Range(y1, y1 + h), cv::Range(x1, x1 + w));
			//生成背板用于粘贴单个mask
			cv::Mat mask_tmp = cv::Mat::zeros(cv::Size(img.cols, img.rows), CV_8UC1);
			masks_3.copyTo(mask_tmp(cv::Range(y1, y1 + h), cv::Range(x1, x1 + w)));
			//赋予color粘到mask上
			add(mask, color_, mask, mask_tmp);
			index += 1;
		}
		cv::Mat out = img + mask * 0.5;
		cv::imshow("results", out);
		cv::waitKey(0);
	}
	
	virtual void saveRes(std::string resRoot, std::string name) {
		auto box_path = resRoot + "\\box";
		auto mask_path = resRoot + "\\mask";
		checkDir(box_path);
		checkDir(mask_path);

		std::string box_save_path = box_path + "\\" + name;
		std::string mask_save_path = mask_path + "\\" + name;

		std::regex reg(R"(\.(\w*)$)");
		box_save_path = std::regex_replace(box_save_path, reg, ".txt");
		mask_save_path = std::regex_replace(mask_save_path, reg, ".bin");

		std::ofstream outputFileB(box_save_path);
		std::ofstream outputFileM(mask_save_path, std::ios::out | std::ios::binary);

		if (!outputFileB.is_open()) {
			std::cout << "Create BOX txt file fail." << std::endl;
		}
		if (!outputFileM.is_open()) {
			std::cout << "Create MASK txt file fail." << std::endl;
		}

		int index = 0;
		for (auto i : this->output_data) {
			for (auto j : i) {
				outputFileB << j << " ";
			}
			outputFileB << "\n";

			auto pic_size = this->protow * this->protoh;
			Eigen::VectorXf v(pic_size);
			v = this->mask_res.row(index);
			outputFileM.write((const char*)(v.data()), pic_size * sizeof(float));
			index++;
		}
		outputFileB.close();
		outputFileM.close();
	}

protected:
	int protoh = 160;
	int protow = 160;
	int mask_stride = 4;
	int mask_channel = 32;
	std::vector<std::vector<float>> allmask;
	void set_ori_out_channles() {
		this->ori_out_channles = { 3 * (this->NOC + 5 + this->mask_channel) };
	}
	std::vector<std::vector<float>> mask_info;
	using YOLO_SEG_RES = std::tuple<int, float, cv::Rect2f, std::vector<float>>;
	std::vector<YOLO_SEG_RES> seg_filter_res, seg_nms_res;
	Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> mask_res;
};

REGISTERPANELCLASS(YoloV5_seg);