#pragma once
#include "YoloSoft.hpp"
#include "factory.hpp"


class  YoloV6_sim :public YoloSoft {
public:
	YoloV6_sim() {}
	~YoloV6_sim() {}

	//先计算各部分数据的指针
	virtual void getPtr(std::vector<std::shared_ptr<icraft::Tensor>> one_head_ptrs) override {
		float* class_data = one_head_ptrs[0]->data<float>().get();
		float* box_data = one_head_ptrs[1]->data<float>().get();
		this->classPtr = class_data + ptrLocate(this->NOC);
		this->boxPtr = box_data + ptrLocate(this->box_info_length);
	};

	virtual std::pair<int, float> getMaxRealScore() override {
		float* max_prob_ptr = std::max_element(this->classPtr, this->classPtr + this->NOC);
		int max_index = std::distance(this->classPtr, max_prob_ptr);
		auto _prob_ = sigmoid(*max_prob_ptr);
		return { max_index, _prob_ };
	}

	virtual float getRealScore(int index) override {
		float* probPtr = this->classPtr + index;
		auto _prob_ = sigmoid(*probPtr);
		return  _prob_;
	}

	virtual std::vector<float> getBbox(int stride, std::vector<float> anchor) override {
		std::vector<float> ltrb;
		for (size_t i = 0; i < 4; i++)
		{
			float* b = this->boxPtr + i;
			ltrb.push_back(*b);
		};

		return S2::_V6(ltrb, this->_w, this->_h, stride);
	}

protected:
	int box_info_length = -1;
	virtual void set_ori_out_channles() override{
		this->box_info_length = 4;
		this->ori_out_channles = { this->NOC,this->box_info_length };
	}

};

REGISTERPANELCLASS(YoloV6_sim);