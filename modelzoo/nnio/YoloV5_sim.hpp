#pragma once
#include "YoloSoft.hpp"
#include "factory.hpp"


class  YoloV5_sim :public YoloSoft {
public:
	YoloV5_sim() {}
	~YoloV5_sim() {}

	//先计算各部分数据的指针
	virtual void getPtr(std::vector<std::shared_ptr<icraft::Tensor>> one_head_ptrs) override {
		float* tensor_data = one_head_ptrs[0]->data<float>().get();
		this->boxPtr = tensor_data + ptrLocate(this->NOC + 5);
		this->scorePtr = this->boxPtr + 4;
		this->classPtr = this->boxPtr + 5;
	};


	virtual std::pair<int, float> getMaxRealScore() override{
		auto _score_ = sigmoid(*this->scorePtr);
		float* max_prob_ptr = std::max_element(this->classPtr, this->classPtr + this->NOC);
		int max_index = std::distance(this->classPtr, max_prob_ptr);
		auto _prob_ = sigmoid(*max_prob_ptr);
		return { max_index,_score_ * _prob_ };
	}

	virtual float getRealScore(int index) override {
		float* probPtr = this->classPtr + index;
		auto _score_ = sigmoid(*this->scorePtr);
		auto _prob_ = sigmoid(*probPtr);
		return _score_ * _prob_;
	}

	virtual std::vector<float> getBbox(int stride, std::vector<float> anchor) override {
		auto xywh = S1::_V5(this->boxPtr);
		S2::_V5(xywh, this->_w, this->_h, stride, anchor);
		return xywh;
	}

protected:
	//配置一个head的输出通道
	virtual void set_ori_out_channles() override {
		this->ori_out_channles = { 3 * (this->NOC + 5) };
	}
};

REGISTERPANELCLASS(YoloV5_sim);