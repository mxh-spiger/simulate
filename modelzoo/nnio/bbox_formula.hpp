#pragma once
#include <vector>
#include "utils.hpp"

// pre realizition of the known models` calc formulas .
// when having a new model, add its formula here or just realize it in its  dirived class.
 
//stage 1 : get raw xywh from tensor
namespace S1{

	// V5==V7
	template <typename T>
	static std::vector<float> _V5(T* tensor_data, int obj_ptr_start, float norm) {
		std::vector<float> res = sigmoid(tensor_data, norm, obj_ptr_start, 4);
		return res;
	}
	
	// V3==V4
	template <typename T>
	static std::vector<float> _V3(T* tensor_data, int obj_ptr_start, float norm) {
		std::vector<float> xy = sigmoid(tensor_data, norm, obj_ptr_start, 2);
		float _w = tensor_data[obj_ptr_start + 2] * norm;
		float _h = tensor_data[obj_ptr_start + 3] * norm;
		return { xy[0],xy[1],_w,_h};
	}

	//for sim
	template <typename T>
	static std::vector<float> _V5(T* tensor_data) {
		std::vector<float> res = sigmoid(tensor_data, 4);
		return res;
	}

	template <typename T>
	static std::vector<float> _V3(T* tensor_data) {
		std::vector<float> xy = sigmoid(tensor_data, 2);
		float _w = tensor_data[2];
		float _h = tensor_data[3];
		return { xy[0],xy[1],_w,_h };
	}



}

//stage 2 : calc real xywh with formula of each virson
namespace S2 {

	// V5==V7
	static void _V5(std::vector<float>& xywh, int location_x,
		int location_y, int stride, std::vector<float> anchor) {
		xywh[0] = (2 * xywh[0] + location_x - 0.5) * stride;
		xywh[1] = (2 * xywh[1] + location_y - 0.5) * stride;
		xywh[2] = 4 * powf(xywh[2], 2) * anchor[0];
		xywh[3] = 4 * powf(xywh[3], 2) * anchor[1];
	}

	// V3==V4
	static void _V3(std::vector<float>& xywh, int location_x,
		int location_y, int stride, std::vector<float> anchor) {
		xywh[0] = (xywh[0] + location_x) * stride;
		xywh[1] = (xywh[1] + location_y) * stride;
		xywh[2] = expf(xywh[2]) * anchor[0];
		xywh[3] = expf(xywh[3]) * anchor[1];
	}


	static std::vector<float> _V6(const std::vector<float>& ltrb, int location_x,
		int location_y, int stride) {
		float x = ((ltrb[2] - ltrb[0]) * 0.5 + location_x + 0.5) * stride;
		float y = ((ltrb[3] - ltrb[1]) * 0.5 + location_y + 0.5) * stride;
		float w = (ltrb[0] + ltrb[2]) * stride;
		float h = (ltrb[1] + ltrb[3]) * stride;
		return { x,y,w,h };
	}

	}




template <typename T>
static T* getTensorData(const std::shared_ptr<icraft::rt::RuntimeTensor> tensor_data, int bits) {
	switch (bits) {
	case 8: {
		int8_t* tensorData = tensor_data->data<int8_t>().get();
		return tensorData;
		break;
	}
	case 16: {
		int16_t* tensorData = tensor_data->data<int16_t>().get();
		return tensorData;
		break;
	}
	default: {
		throw "wrong bits num!";
		exit(EXIT_FAILURE);
	}
	}
}