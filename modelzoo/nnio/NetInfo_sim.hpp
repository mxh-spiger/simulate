#pragma once
#include "runtime.h"
#include <vector>
#include <string>
#include "opencv2/opencv.hpp"
#include "simulator_network.h"
#include <icraft-ir/input.h>
#include "simulator.h"

//由于simulator接口正在与runtime进行整合，这里单独为simulator做一个获取netinfo的hpp，暂时不在nnio的netinfo中做重载

template <typename T>
static std::vector<T> getMapValue(std::map<int, std::vector<T>> imap) {
    auto x = imap.begin();
    auto y = *x;
    auto c = y.second;
    return c;
};

class  NetInfo_sim {
public:
    int bits;
    std::vector<int> stride = {};
    std::vector<std::vector<float>> normalratio = {};
    std::vector<std::vector<std::pair<int, int>>> chann_distr{};
    std::vector<std::vector<std::pair<int, int>>> scale = {};
    std::pair<int, int> input_hw;
    std::vector<int> inputshape;  // h w c
    std::vector<std::string> hardOpNames = {};
    bool imk = false;
    bool icorepost = false;
    std::string stage_name;
    std::vector<std::shared_ptr<icraft::ir::FeatureMap>> inputs;
    std::list<std::shared_ptr<icraft::ir::Operation>> opList;

    NetInfo_sim(std::shared_ptr<icraft::sim::SimulatorNetwork> network_ptr) {
        this->opList = network_ptr->getOps();
        this->stage_name = network_ptr->stageName();
        //std::cout << "stage name=" << stage_name << '\n';
        this->inputshape = this->getInputShape();
        this->input_hw = { inputshape[0],inputshape[1] };
        //this->hardOpNames = this->getHardOpName(network_ptr);
        //this->imk = std::find(this->hardOpNames.begin(), this->hardOpNames.end(), "ImageMake") != this->hardOpNames.end();
        //this->icorepost = std::find(this->hardOpNames.begin(), this->hardOpNames.end(), "IcorePost") != this->hardOpNames.end();
        this->infoFromOutput();

    }

    void infoFromOutput() {
        auto op_list = this->opList;
        auto it = op_list.crbegin();
        auto output = *(it++);
        auto inputs_num = output->inputFtmp().size();
        auto inputs = output->inputFtmp();
        for (auto input : inputs) {
            auto dims = input->dim();
            if ((this->stage_name == "LAYOUTED") || (this->stage_name == "GENERATED")) {
                dims = { dims[0],dims[2],dims[3], dims[1] * dims[4] };
            }
            //auto _dims = { dims[0],dims[1],dims[2], dims[3] }; //这里认为输出无效通道是在有效通道后的情况，没考虑间隔排布
            int _stride = this->inputshape[0] / dims[1];
            this->stride.push_back(_stride);
            this->bits = input->typeBits();
            if ((this->stage_name != "PARSED") && (this->stage_name != "OPTIMIZED")) {
                this->scale.push_back(input->scale());
                this->normalratio.push_back(input->normratio());
                this->chann_distr.emplace_back(input->channDistribution());              
            }
        }
    }

   /* std::vector<std::string> getHardOpName(const std::shared_ptr<icraft::sim::SimulatorNetwork> network_ptr) {
        std::vector<std::string> hardOpNames = {};
        auto dllmap = network_ptr->dlls();
        for (auto iter = dllmap.begin(); iter != dllmap.end(); iter++) {
            std::string dllname = iter->first;
            hardOpNames.push_back(dllname);
        }
        return hardOpNames;
    }*/

    std::vector<int> getInputShape() {
        auto stage = this->stage_name;
        auto op_list = this->opList;
        auto it = op_list.cbegin();
        auto input = *it;        // 第一个op
        auto inputImg = input->outputFtmp();
        this->inputs = inputImg;
        if ((stage == "LAYOUTED")) {
            int channel = inputImg[0]->channDistribution()[0].second;
            return { inputImg[0]->dim()[1], inputImg[0]->dim()[2] ,channel };
        }

        if ((stage == "GENERATED")) {
            int channel = inputImg[0]->channDistribution()[0].second;
            return { inputImg[0]->dim()[2], inputImg[0]->dim()[3] ,channel };
        }

        return { inputImg[0]->dim()[1], inputImg[0]->dim()[2] , inputImg[0]->dim()[3] };

    }

    std::shared_ptr<icraft::Tensor> maketensor(const cv::Mat& m) {
        cv::Mat converted = m;
        m.convertTo(converted, CV_32F);
        auto N = 1;
        auto H = converted.rows;
        auto W = converted.cols;
        auto C = converted.channels();
        auto size = N * H * W * C;
        std::vector<int> dims{ N, H, W, C };
        std::shared_ptr<float[]> data(new float[size]);
        std::copy_n((float*)converted.data, size, data.get());
    
        auto op_list = this->opList;
        auto it = op_list.cbegin();
        auto input = std::dynamic_pointer_cast<icraft::ir::Input>(*it);
        auto mean = input->preMeans();
        auto scale = input->preScales();
        auto swap = input->channelSwaps();
        icraft::Tensor input_tensor = icraft::Tensor(data, { 1, H, W, C });
        auto ff_ops = icraft::sim::Simulator::getFFOps();
        input_tensor = ff_ops->Input(input_tensor, getMapValue(scale), getMapValue(mean), getMapValue(swap));
        auto cv_tensor = std::make_shared<icraft::Tensor>(input_tensor);
        
        if (this->stage_name == "LAYOUTED") {
            cv_tensor.get()->channnelMakeup(this->inputs[0]->channDistribution());
        }

        if (this->stage_name == "GENERATED") {												    
            cv_tensor.get()->toBYLayout(this->inputs[0]->dim(), this->inputs[0]->channDistribution());         
        }

        return cv_tensor;
    }
    
    void dataFormatH2S(std::vector<icraft::Tensor>& result_tensor) {
        if (this->stage_name == "QUANTIZED") {
            for (size_t i = 0; i < result_tensor.size(); ++i) {
            result_tensor[i].dequantize(this->normalratio[i], this->scale[i], this->chann_distr[i]);
            }
        }

        if ((this->stage_name == "LAYOUTED")) {
            for (size_t i = 0; i < result_tensor.size(); ++i) {
                result_tensor[i].dequantize(this->normalratio[i], this->scale[i], this->chann_distr[i]);
                result_tensor[i].dechannnelMakeup(this->chann_distr[i]);
            }
        }

        if ((this->stage_name == "GENERATED")) {
            for (size_t i = 0; i < result_tensor.size(); ++i) {
                result_tensor[i].dequantize(this->normalratio[i], this->scale[i], this->chann_distr[i]);
                result_tensor[i].toNormalLayout(this->chann_distr[i]);
            }
        }   
    }

};
