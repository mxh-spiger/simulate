#pragma once
#include "YoloSoft.hpp"
#include "factory.hpp"


class  YoloX_sim :public YoloSoft {
public:
	YoloX_sim() {}
	~YoloX_sim() {}

	//先计算各部分数据的指针
	virtual void getPtr(std::vector<std::shared_ptr<icraft::Tensor>> one_head_ptrs) override {
		float* score_data = one_head_ptrs[0]->data<float>().get();
		float* box_data = one_head_ptrs[1]->data<float>().get();
		float* class_data = one_head_ptrs[2]->data<float>().get();
		
		this->scorePtr = score_data+ ptrLocate(1);
		this->boxPtr = box_data + ptrLocate(this->box_info_length);
		this->classPtr = class_data + ptrLocate(this->NOC);
		
	};

	virtual std::pair<int, float> getMaxRealScore() override {
		auto _score_ = sigmoid(*this->scorePtr);
		float* max_prob_ptr = std::max_element(this->classPtr, this->classPtr + this->NOC);
		int max_index = std::distance(this->classPtr, max_prob_ptr);
		auto _prob_ = sigmoid(*max_prob_ptr);
		return { max_index,_score_ * _prob_ };
	}

	virtual float getRealScore(int index) override {
		float* probPtr = this->classPtr + index;
		auto _score_ = sigmoid(*this->scorePtr);
		auto _prob_ = sigmoid(*probPtr);
		return _score_ * _prob_;
	}

	virtual std::vector<float> getBbox(int stride, std::vector<float> anchor) override {
		std::vector<float> xywh;
		for (size_t i = 0; i < 4; i++)
		{
			float* b = this->boxPtr + i;
			xywh.push_back(*b);
		};
		xywh[0] = (xywh[0] + this->_w) * stride;
		xywh[1] = (xywh[1] + this->_h) * stride;
		xywh[2] = expf(xywh[2]) * stride;
		xywh[3] = expf(xywh[3]) * stride;

		return xywh;
	}

protected:
	int box_info_length = -1;
	virtual void set_ori_out_channles() override {
		this->box_info_length = 4;
		this->ori_out_channles = { 1,this->box_info_length,this->NOC };
	}

};

REGISTERPANELCLASS(YoloX_sim);