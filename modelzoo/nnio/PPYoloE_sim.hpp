#pragma once
#include "YoloV8_sim.hpp"
#include "factory.hpp"


class  PPYoloE_sim :public YoloV8_sim {
public:
	PPYoloE_sim() {}
	~PPYoloE_sim() {}

	virtual void coordTrans(PicPre& img, bool check_border = true) override{
		float ratio_h = img.getRatio().first;
		float ratio_w = img.getRatio().second;
		for (auto&& res : this->nms_res) {
			float class_id = std::get<0>(res);
			float score = std::get<1>(res);
			auto box = std::get<2>(res);
			float x1 = (box.tl().x) / ratio_w;
			float y1 = (box.tl().y) / ratio_h;
			float x2 = (box.br().x) / ratio_w;
			float y2 = (box.br().y) / ratio_h;
			if (check_border) {
				x1 = checkBorder(x1, 0.f, (float)img.src_img.cols);
				y1 = checkBorder(y1, 0.f, (float)img.src_img.rows);
				x2 = checkBorder(x2, 0.f, (float)img.src_img.cols);
				y2 = checkBorder(y2, 0.f, (float)img.src_img.rows);
			}
			float w = x2 - x1;
			float h = y2 - y1;
			//bbox�����Ͻǵ��wh
			this->output_data.emplace_back(std::vector<float>({ class_id, x1, y1, w, h, score }));
		}
	}

protected:
	virtual void set_ori_out_channles() override {
		this->box_info_length = 68;
		this->ori_out_channles = { this->NOC,this->box_info_length };
	}

};

REGISTERPANELCLASS(PPYoloE_sim);