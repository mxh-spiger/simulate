#pragma once
#include <vector>
#include "Yolo.hpp"
#include "opencv2/opencv.hpp"
#include "runtime.h"
#include "NetInfo_sim.hpp"
#include "bbox_formula.hpp"

class  YoloSoft :public Yolo {
public:
	YoloSoft() {}
	virtual ~YoloSoft() {}
	//soft filter
	void filter(const std::vector<icraft::Tensor>& res) {
		int res_num = res.size();
		int extra_res_num = res.size() % this->parts;

		//将输出的ftmp指针按head分组，多余的部分一组
		for (int i = 0; i < res_num-extra_res_num; i+=this->parts) {
			OneHeadPtrs each_head;
			for (int j = 0; j < this->parts; j++)
			{
				int id = i + j;
				each_head.emplace_back(std::make_shared<icraft::Tensor>(res[id]));
			}
			this->results.emplace_back(each_head);
		}
		if (extra_res_num != 0) {
			OneHeadPtrs extra_head;
			for (int i = res_num - extra_res_num; i < res.size() % this->parts; i++)
			{
				extra_head.emplace_back(std::make_shared<icraft::Tensor>(res[i]));
			}
			this->results.emplace_back(extra_head);
		}

		//按head遍历，获取每个head的指针vector，以及一些必要信息，传给oneobj计算一个目标的信息
		for (size_t head_index = 0; head_index < this->NOH; head_index++) {
			this->_H = results[head_index][0]->dims()[1];
			this->_W = results[head_index][0]->dims()[2];
			OneHeadPtrs one_head_ptrs = this->results[head_index];
			for (size_t h = 0; h < this->_H; h++) {
				this->_h = h;
				for (size_t w = 0; w < this->_W; w++) {
					this->_w = w;
					for (size_t anchor_index = 0; anchor_index < this->NOA; anchor_index++) {
						this->_acid = anchor_index;
						auto one_head_stride = this->_stride[head_index];
						std::vector<float>one_head_anchor = {};
						if (this->ANCHORS.size() != 0) {
							one_head_anchor = this->ANCHORS[head_index][anchor_index];
						}
						this->oneObj(one_head_ptrs,one_head_stride, one_head_anchor);
						
					}
				}
			}
		}
	}

	//计算一个目标的信息，通过getPtr计算socre，prob，bbox信息的指针，然后用指向的原始数据计算
	void oneObj(std::vector<std::shared_ptr<icraft::Tensor>> one_head_ptrs, 
		int stride,std::vector<float> anchor) {
		this->getPtr(one_head_ptrs);
		if (!this->MULTILABLE) {
			auto i_s = this->getMaxRealScore();
			auto max_index = i_s.first;
			auto realscore = i_s.second;
			if (realscore > this->CONF) {
				auto bbox = this->getBbox( stride, anchor);
				this->id_list.emplace_back(max_index);
				this->socre_list.emplace_back(realscore);
				this->box_list.emplace_back(cv::Rect2f((bbox[0] - bbox[2] / 2),
					(bbox[1] - bbox[3] / 2), bbox[2], bbox[3]));
			}
		}
		else {
			for (size_t i = 0; i < this->NOC; i++) {
				auto realscore = this->getRealScore( i);
				if (realscore > this->CONF) {
					auto bbox = this->getBbox(stride, anchor);
					this->id_list.emplace_back(i);
					this->socre_list.emplace_back(realscore);
					this->box_list.emplace_back(cv::Rect2f((bbox[0] - bbox[2] / 2),
						(bbox[1] - bbox[3] / 2), bbox[2], bbox[3]));
				}
			}
		}
	}

	virtual void init(extCfg& cfg, NetInfo_sim& netinfo) {
		this->CONF = cfg.CONF;
		this->IOU = cfg.IOU;
		this->NOC = cfg.NOC;
		this->NOH = cfg.NOH;
		this->ANCHORS = cfg.ANCHORS;
		this->MULTILABLE = cfg.MULTILABLE;
		if (cfg.ANCHORS.size()) {
			this->NOA = this->ANCHORS[0].size();
		}
		this->CONF_arcsigmoid = arcSigmoid(this->CONF);
		this->stride = netinfo.stride;
		this->set_ori_out_channles();
		this->set_parts();
		this->set_stride_by_head();
	}

	//init print
	virtual void init_debug() {
		std::cout << "conf: " << this->CONF << std::endl;
		std::cout << "iou: " << this->IOU << std::endl;
		for (auto i : this->normalratio)
		{
			std::cout << "norm: " << i << std::endl;
		}

		for (auto i : this->ori_out_channles)
		{
			std::cout << "ori o c: " << i << std::endl;
		}
		for (auto i : this->_norm)
		{
			for (auto j : i)
			{
				std::cout << "intri norm: " << j << std::endl;
			}
		}
		for (auto i : this->_stride)
		{
			std::cout << "stride: " << i << std::endl;
		}
		std::cout << "parts: " << this->parts << std::endl;
		std::cout << "noc: " << this->NOC << std::endl;
	};


	virtual std::pair<int, float> getMaxRealScore() = 0;
	
	virtual float getRealScore(int index) = 0;

	virtual std::vector<float> getBbox(int stride, std::vector<float> anchor) = 0;

	virtual void getPtr(std::vector<std::shared_ptr<icraft::Tensor>> one_head_ptrs) = 0;
	
	virtual int ptrLocate(int one_anchor_info_length ) {
		return this->_h* this->_W* one_anchor_info_length * this->NOA
			+ this->_w * one_anchor_info_length * this->NOA
			+ this->_acid * one_anchor_info_length;
	}

	

protected:
	float* classPtr = NULL;
	float* boxPtr = NULL;
	float* scorePtr = NULL;
	using OneHeadPtrs = std::vector<std::shared_ptr<icraft::Tensor>>;
	std::vector<OneHeadPtrs> results;
	int _w, _h, _W, _H, _acid = -1;
};

