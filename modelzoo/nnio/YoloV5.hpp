#pragma once
#include "YoloHard.hpp"
#include "factory.hpp"


class  YoloV5 :public YoloHard {
public:
	YoloV5() {}
	~YoloV5() {}
	template<typename T>
	float grs(T* tensor_data,
		int obj_ptr_start, std::vector<float> norm, int index) {
		auto _score_ = sigmoid(tensor_data[obj_ptr_start + 4] * norm[0]);
		auto _prob_ = sigmoid(tensor_data[obj_ptr_start + 5 + index] * norm[0]);
		return _score_ * _prob_;
	}
	
	template<typename T>
	std::pair<int, float> gmrs(T* tensor_data, int obj_ptr_start, std::vector<float> norm) {
		auto _score_ = sigmoid(tensor_data[obj_ptr_start + 4] * norm[0]);
		T* class_ptr_start = tensor_data + obj_ptr_start + 5;
		T* max_prob_ptr = std::max_element(class_ptr_start, class_ptr_start + this->NOC);
		int max_index = std::distance(class_ptr_start, max_prob_ptr);
		auto _prob_ = sigmoid(*max_prob_ptr * norm[0]);
		return { max_index,_score_ * _prob_ };
	}

	template<typename T>
	std::vector<float> gb(T* tensor_data,
		std::vector<float> norm, int obj_ptr_start, int location_x, int location_y,
		int stride, std::vector<float> anchor) {
		auto xywh = S1::_V5(tensor_data, obj_ptr_start, norm[0]);
		S2::_V5(xywh, location_x, location_y, stride, anchor);
		return xywh;
	}


	virtual float getRealScore(int8_t* tensor_data,
		int obj_ptr_start, std::vector<float> norm, int index)  {

		auto s = (this->bits == 16) ? this->grs((int16_t*)tensor_data, obj_ptr_start
			, norm, index) : this->grs(tensor_data, obj_ptr_start, norm, index);

		return s;
	}

	
	virtual std::pair<int, float> getMaxRealScore(int8_t* tensor_data, int obj_ptr_start,std::vector<float> norm)  {
		
		auto s = (this->bits == 16) ? this->gmrs((int16_t*)tensor_data, obj_ptr_start
			, norm) : this->gmrs(tensor_data, obj_ptr_start, norm);

		return s;
	}

	
	virtual std::vector<float> getBbox(int8_t* tensor_data,
		std::vector<float> norm,int obj_ptr_start, int location_x, int location_y, 
		int stride, std::vector<float> anchor)  {
		auto xywh = (this->bits == 16) ? this->gb((int16_t*)tensor_data, norm
			, obj_ptr_start, location_x, location_y, stride, anchor) : this->gb(tensor_data, norm
				, obj_ptr_start, location_x, location_y, stride, anchor);
		return xywh;
	}


protected:
	virtual void set_ori_out_channles() {
		this->ori_out_channles = { 3 * (this->NOC + 5) };
	}
};

REGISTERPANELCLASS(YoloV5);