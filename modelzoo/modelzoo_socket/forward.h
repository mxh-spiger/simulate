#pragma once
#include "yaml-cpp/yaml.h"
#include "opencv2/opencv.hpp"
#include "runtime.h"
#include "PicPre.hpp"

#define FFYOLO(classname)\
static void classname##_ff(const YAML::Node& network, const YAML::Node& param) {\
	return yolo(network, param, #classname);\
}\

void yolo(const YAML::Node& network, const YAML::Node& param, std::string classname,
	int inputFlag = PicPre::ImreadModes::IMREAD_COLOR);

void ppyoloe(const YAML::Node& network, const YAML::Node& param, std::string classname,
	int inputFlag = PicPre::ImreadModes::IMREAD_COLOR);

void byteTrack(const YAML::Node& network, const YAML::Node& param, std::string classname);

void semanticSeg(const YAML::Node& network, const YAML::Node& param);

void rdn(const YAML::Node& network, const YAML::Node& param);

void classify(const YAML::Node& network, const YAML::Node& param, const int inputFlag);

static void classify_color(const YAML::Node& network, const YAML::Node& param) {
	return classify(network, param, PicPre::ImreadModes::IMREAD_COLOR);
};

static void PPYoloE_ff(const YAML::Node& network, const YAML::Node& param) {
	return ppyoloe(network, param, "PPYoloE");
}

void yolo_seg_2stage(const YAML::Node& network, const YAML::Node& param, int inputFlag = PicPre::ImreadModes::IMREAD_COLOR);
static void YoloSeg_2stage_ff(const YAML::Node& network, const YAML::Node& param) {
	return yolo_seg_2stage(network, param);
}

/*
* 函数注册：
* 由于函数与类存在依赖关系，在对应的类hpp中用工厂的方式注册函数指针map会引起交叉引用，
* 目前还没有花时间解决这个问题，只能先在这边手动注册
*/
FFYOLO(YoloV5);
FFYOLO(YoloV3);
FFYOLO(YoloX);
FFYOLO(YoloV8);
FFYOLO(YoloV6);
FFYOLO(YoloV5_obb);
FFYOLO(YoloV5_pose);
FFYOLO(YoloV8_pose);
FFYOLO(YoloV5_seg);

static void YoloV5_gray_ff(const YAML::Node& network, const YAML::Node& param) {
	return yolo(network, param, "YoloV5", PicPre::ImreadModes::IMREAD_GRAYSCALE);
}

static void ByteTrack_ff(const YAML::Node& network, const YAML::Node& param) {
	return byteTrack(network, param, "YoloX");
}

/*
* 添加说明：
①string部分可以自定义，要与yaml中调用的名字一致
②函数指针的名字，由FFYOLO(classname)中的类名决定，既:classname##_ff
*/
using model_forward = void(*) (const YAML::Node& network, const YAML::Node& param);
static std::map<std::string, model_forward>  funcMap = {
	{"yolov5",YoloV5_ff},
	{"yolov5_gray",YoloV5_gray_ff},            //未测
	{"yolov3",YoloV3_ff},
	{"yolox",YoloX_ff},
	{"yolov8",YoloV8_ff},
	{"ppyoloe",PPYoloE_ff},                    //未测
	{"yolov6",YoloV6_ff},
	{"yolov5_obb",YoloV5_obb_ff},
	{"yolov5_pose",YoloV5_pose_ff},
	{"yolov8_pose",YoloV8_pose_ff},
	{"yolov5_seg",YoloV5_seg_ff},
	{"bytetrack",ByteTrack_ff},
	{"semanticSeg",semanticSeg},
	{"rdn",rdn},
	{"classify_color",classify_color},
	{"yolo_seg_2stage",YoloSeg_2stage_ff}
};

