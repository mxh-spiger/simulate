#include "runtime.h"
#include "icraft-ir/hard_op.h"
#include "fmt/core.h"
#include "yaml-cpp/yaml.h"
#include "PicPre.hpp"
#include "utils.hpp"
#include "NetInfo.hpp"
#include "forward.h"
#include "factory.hpp"
#include "YoloV5.hpp"
#include "YoloV3.hpp"
#include "YoloX.hpp"
#include "YoloV8.hpp"
#include "YoloV6.hpp"
#include "YoloV5_obb.hpp"
#include "YoloV5_pose.hpp"
#include "YoloV8_pose.hpp"
#include "YoloV5_seg.hpp"
#include "PPYoloE.hpp"
#include "BYTETracker.h"
#include "seg.hpp"
#include "YoloV5_seg_2stage.hpp"

void yolo(const YAML::Node& network, const YAML::Node& param, std::string classname, const int inputFlag) {

    //-------------icraft config-----------------------//
    std::string url = network["url"].as<std::string>();
    std::string jsonPath = network["jsonPath"].as<std::string>();
    std::string rawPath = network["rawPath"].as<std::string>();
    std::string resRoot = network["resRoot"].as<std::string>();
    bool show = network["show"].as<bool>();
    checkDir(resRoot);
    //--------------network config-----------------------//
    Yolo::extCfg cfg;
    // post relate
    cfg.CONF = param["conf"].as<float>();
    cfg.IOU = param["iou"].as<float>();
    cfg.NOC = param["number_of_class"].as<int>();
    cfg.NOH = param["number_of_head"].as<int>();
    cfg.ANCHORS = param["anchors"].as<std::vector<std::vector<std::vector<float>>>>();
    cfg.MULTILABLE = param["multilable"].as<bool>();

    // dataset relate
    std::string imgRoot = param["imgRoot"].as<std::string>();
    std::string imgList = param["imgList"].as<std::string>();
    std::string names_path = param["names"].as<std::string>();
    auto names = toVector(names_path);

    //-------------SET ICORE-----------------------//
    auto device = icraft::dev::IcraftDeviceManager::open(url.data());
    auto network_ptr = std::make_shared<icraft::rt::RuntimeNetwork>(jsonPath.data(), rawPath.data());
    icraft::rt::Runtime runtime(network_ptr, device);
    NetInfo netinfo(network_ptr);
    runtime.speedMode();
    //runtime.compressFtmpSegment();
    //runtime.setCalcTime();
    runtime.apply();

    //-----支持network view模式，既任意算子段执行-----   
    auto ops = network_ptr->getOps();
    //如果模型编译时使用了imagemake，则网络第一个input算子可以去掉不用执行，减少运行时间
    auto runtime_view = runtime.view(ops.begin() + 1, ops.end());

    //------------- PICS -----------------------//
    int index = 0;
    auto namevector = toVector(imgList);
    int totalnum = namevector.size();

    //申请一块ps ddr的预留空间（目前是用u-dma-buf,既linux内核空间一块连续内存cma）用来存放tensor
    auto chunck = netinfo.requestMem(device);
    for (auto name : namevector) {
        progress(index, totalnum);
        index++;
        //-------------PRE PROCESS-----------------------//
        std::string img_path = imgRoot + "\\" + name;
        PicPre img(img_path, inputFlag);
        img.Resize(netinfo.input_hw, PicPre::LONG_SIDE).rPad();

        //-------------ICRAFT RUN-----------------------//
        //用cvmat构造runtime tensor
        auto img_tensor = netinfo.cvMat2Tensor(img.dst_img);
        //初始化dma模块，并把tensor从用户空间拷贝到预留的空间
        netinfo.dmaInit(device, img_tensor, chunck);
        //启动dma，开始讲数据从ps ddr搬移到pl ddr。imagemake硬算子在此数据流上实时处理，如果不用则直接由dma搬移。
        netinfo.dmaStart(device);
        //自动完成上述步骤，不过申请的psddr空间会在函数内部释放 ，这一点对模型库跑单模型无影响
        //auto img_tensor = netinfo.maketensor(device, img.dst_img);

        //执行前向
        auto result_tensor = runtime_view->forward({ img_tensor });
        
        //也可以直接使用runtime
        //auto result_tensor = runtime.forward({ img_tensor }); 

        //-------------POST PROCESS-----------------------//
        auto yolo = (YoloHard*)ObjectFactory::GetInstance()->CreateObject(classname);
        yolo->init(cfg, netinfo);
        //yolo->init_debug();
        yolo->filter(result_tensor);
        yolo->nms();
        yolo->coordTrans(img);
        if (show) {
            yolo->visualize(img.src_img, names);
        }
        yolo->saveRes(resRoot, name);
        delete yolo;
        runtime.reset();
    }
    //申请的ps ddr预留空间彻底不用后可以释放掉
    chunck->free();
    icraft::dev::IcraftDeviceManager::close(device);
}

void ppyoloe(const YAML::Node& network, const YAML::Node& param, std::string classname, const int inputFlag) {

    //-------------icraft config-----------------------//
    std::string url = network["url"].as<std::string>();
    std::string jsonPath = network["jsonPath"].as<std::string>();
    std::string rawPath = network["rawPath"].as<std::string>();
    std::string resRoot = network["resRoot"].as<std::string>();
    bool show = network["show"].as<bool>();
    checkDir(resRoot);
    //--------------network config-----------------------//
    Yolo::extCfg cfg;
    // post relate
    cfg.CONF = param["conf"].as<float>();
    cfg.IOU = param["iou"].as<float>();
    cfg.NOC = param["number_of_class"].as<int>();
    cfg.NOH = param["number_of_head"].as<int>();
    cfg.ANCHORS = param["anchors"].as<std::vector<std::vector<std::vector<float>>>>();
    cfg.MULTILABLE = param["multilable"].as<bool>();

    // dataset relate
    std::string imgRoot = param["imgRoot"].as<std::string>();
    std::string imgList = param["imgList"].as<std::string>();
    std::string names_path = param["names"].as<std::string>();
    auto names = toVector(names_path);

    //-------------SET ICORE-----------------------//
    auto device = icraft::dev::IcraftDeviceManager::open(url.data());
    auto network_ptr = std::make_shared<icraft::rt::RuntimeNetwork>(jsonPath.data(), rawPath.data());
    icraft::rt::Runtime runtime(network_ptr, device);
    NetInfo netinfo(network_ptr);
    //runtime.speedMode();
    //runtime.setCalcTime(true); 
    runtime.apply();

    //-----支持network view模式，既任意算子段执行-----
    auto ops = network_ptr->getOps();
    auto runtime_view = runtime.view(ops.begin()+1, ops.end()); 

    //------------- PICS -----------------------//
    int index = 0;
    auto namevector = toVector(imgList);
    int totalnum = namevector.size();
    for (auto name : namevector) {
        progress(index, totalnum);
        index++;
        //-------------PRE PROCESS-----------------------//
        std::string img_path = imgRoot + "\\" + name;
        PicPre img(img_path, inputFlag);
        img.Resize(netinfo.input_hw, PicPre::BOTH_SIDE);

        //-------------ICRAFT RUN-----------------------//
        auto img_tensor = netinfo.maketensor(device, img.dst_img);
        //auto result_tensor = runtime.forward({ img_tensor });
        auto result_tensor = runtime_view->forward({ img_tensor });

        //-------------POST PROCESS-----------------------//
        auto yolo = (YoloHard*)ObjectFactory::GetInstance()->CreateObject(classname);
        yolo->init(cfg, netinfo);
        //yolo->init_debug();
        yolo->filter(result_tensor);
        yolo->nms();
        yolo->coordTrans(img);
        if (show) {
            yolo->visualize(img.src_img, names);
        }
        yolo->saveRes(resRoot, name);
        delete yolo;
        runtime.reset();
    }
    //icraft::dev::IcraftDeviceManager::close(device);
}

void byteTrack(const YAML::Node& network, const YAML::Node& param, std::string classname) {
    //-------------icraft config-----------------------//
    std::string url = network["url"].as<std::string>();
    std::string jsonPath = network["jsonPath"].as<std::string>();
    std::string rawPath = network["rawPath"].as<std::string>();
    std::string resRoot = network["resRoot"].as<std::string>();
    bool show = network["show"].as<bool>();
    checkDir(resRoot);
    //--------------network config-----------------------//
    Yolo::extCfg cfg;
    // post relate
    cfg.CONF = param["conf"].as<float>();
    cfg.IOU = param["iou"].as<float>();
    cfg.NOC = param["number_of_class"].as<int>();
    cfg.NOH = param["number_of_head"].as<int>();
    cfg.ANCHORS = param["anchors"].as<std::vector<std::vector<std::vector<float>>>>();
    cfg.MULTILABLE = param["multilable"].as<bool>();

    // dataset relate
    std::string videoRoot = param["videoRoot"].as<std::string>();
    std::string videoList = param["videoList"].as<std::string>();
    std::string names_path = param["names"].as<std::string>();
    auto names = toVector(names_path);

    //-------------SET ICORE-----------------------//
    auto device = icraft::dev::IcraftDeviceManager::open(url.data());
    auto network_ptr = std::make_shared<icraft::rt::RuntimeNetwork>(jsonPath.data(), rawPath.data());
    icraft::rt::Runtime runtime(network_ptr, device);
    NetInfo netinfo(network_ptr);
    //runtime.speedMode();
    //runtime.setCalcTime(true); 
    runtime.apply();

    //-----支持network view模式，既任意算子段执行-----
    auto ops = network_ptr->getOps();
    auto runtime_view = runtime.view(ops.begin()+1, ops.end());

    //------------- PICS -----------------------//
    int index = 0;
    auto namevector = toVector(videoList);
    int totalnum = namevector.size();
    for (auto name : namevector) {
        //-------------PRE PROCESS-----------------------//
        std::string video_path = videoRoot + "\\" + name;
        cv::VideoCapture cap(video_path);
        cv::Mat one_frame;
        int fps = cap.get(cv::CAP_PROP_FPS);
        BYTETracker tracker(fps, 30);
        std::vector<std::array<float, 10>> res_export = std::vector<std::array<float, 10>>();
        auto total_frames = cap.get(cv::CAP_PROP_FRAME_COUNT);
        for (int frame_id = 0; frame_id < total_frames; ++frame_id) {
            progress(frame_id, total_frames);
            cap >> one_frame;
            PicPre img(one_frame);
            img.Resize(netinfo.input_hw, PicPre::LONG_SIDE).rPad();
            //cv::imshow(" ", img.dst_img);
            //cv::waitKey(0);
            //-------------ICRAFT RUN-----------------------//
            auto img_tensor = netinfo.maketensor(device, img.dst_img);
            //auto result_tensor = runtime.forward({ img_tensor });
            auto result_tensor = runtime_view->forward({ img_tensor });

            //-------------POST PROCESS-----------------------//
            auto yolo = (YoloHard*)ObjectFactory::GetInstance()->CreateObject(classname);
            yolo->init(cfg, netinfo);
            //yolo->init_debug();
            yolo->filter(result_tensor);
            yolo->nms();
            yolo->coordTrans(img, false);
            //检测结果可视化
            //yolo->visualize(img.src_img, names);
            auto res = yolo->getOutputData();
            std::vector<Object> objects;
            for (auto i : res) {
                Object obj;
                obj.rect.x = i[1];
                obj.rect.y = i[2];
                obj.rect.width = i[3];
                obj.rect.height = i[4];
                obj.label = 0;
                obj.prob = i[5];
                objects.push_back(obj);
            }
            std::vector<STrack> output_stracks = tracker.update(objects);

            for (int i = 0; i < output_stracks.size(); i++) {
                auto frame = (float)(frame_id)+1;
                auto id = (float)(output_stracks[i].track_id);
                std::vector<float> tlwh = output_stracks[i].tlwh;
                bool vertical = tlwh[2] / tlwh[3] > 1.6;
                if (tlwh[2] * tlwh[3] > 20 && !vertical) {
                    auto bb_left = tlwh[0];
                    auto bb_top = tlwh[1];
                    auto bb_width = tlwh[2];
                    auto bb_height = tlwh[3];
                    auto conf = output_stracks[i].score;
                    std::array<float, 10> each_line = { frame, id, bb_left, bb_top, bb_width, bb_height, conf, -1.f, -1.f, -1.f };
                    res_export.push_back(each_line);
                    if (show) {
                        Scalar s = tracker.get_color(output_stracks[i].track_id);
                        putText(one_frame, format("%.0f", id), Point(bb_left, bb_top), 0, 0.6, Scalar(0, 0, 255), 2, LINE_AA);
                        cv::rectangle(one_frame, Rect(bb_left, bb_top, bb_width, bb_height), s, 2);
                    }
                }
            }
            if (show) {
                cv::imshow(name, one_frame);
                cv::waitKey(1);
            }
            /*yolo->saveRes(resRoot, name);*/
            delete yolo;
            runtime.reset();

        }
        std::string result_file = resRoot + "\\" + name + ".txt";
        std::fstream outFile(result_file, std::ios::out | std::ios::trunc);
        for (size_t line = 0; line < res_export.size(); ++line) {
            for (size_t i = 0; i < 10; ++i) {
                outFile << res_export[line][i];
                if (i != 9) {
                    outFile << ",";
                }
            }
            outFile << "\n";
        }
        outFile.close();
        progress(index, totalnum);
        index++;
    }
    //icraft::dev::IcraftDeviceManager::close(device);
}

void semanticSeg(const YAML::Node& network, const YAML::Node& param) {
    // 语义分割模型 DDRNet socket  
    //-------------icraft config-----------------------//
    std::string url = network["url"].as<std::string>();
    std::string jsonPath = network["jsonPath"].as<std::string>();
    std::string rawPath = network["rawPath"].as<std::string>();
    std::string resRoot = network["resRoot"].as<std::string>();
    bool show = network["show"].as<bool>();
    checkDir(resRoot);

    // dataset relate
    std::string imgRoot = param["imgRoot"].as<std::string>();
    std::string imgList = param["imgList"].as<std::string>();
    std::string dataset_name = param["dataset_name"].as<std::string>();

    //-------------SET ICORE-----------------------//
    auto device = icraft::dev::IcraftDeviceManager::open(url.data());
    auto network_ptr = std::make_shared<icraft::rt::RuntimeNetwork>(jsonPath.data(), rawPath.data());
    icraft::rt::Runtime runtime(network_ptr, device);
    NetInfo netinfo(network_ptr);
    runtime.speedMode();
    //runtime.setCalcTime(true); 
    runtime.apply();

    //-----支持network view模式，既任意算子段执行-----
    auto ops = network_ptr->getOps();
    auto runtime_view = runtime.view(ops.begin()+1, ops.end());

    //------------- PICS -----------------------//
    int index = 0;
    auto namevector = toVector(imgList);
    int totalnum = namevector.size();
    for (auto name : namevector) {
        progress(index, totalnum);
        index++;
        //-------------PRE PROCESS-----------------------//
        std::string img_path = imgRoot + "\\" + name;
        PicPre img(img_path);
        img.Resize(netinfo.input_hw, PicPre::BOTH_SIDE);

        //-------------ICRAFT RUN-----------------------//
        auto img_tensor = netinfo.maketensor(device, img.dst_img);

        //auto result_tensor = runtime.forward({ img_tensor });
        auto result_tensor = runtime_view->forward({ img_tensor });

        //-------------POST PROCESS-----------------------//

        // 硬件排布转换为软件排布
        dataFormatH2S(result_tensor, device, netinfo.infos);

        auto dims = result_tensor[0]->dims();
        cv::Mat net_result_mat = cv::Mat(dims[1], dims[2], CV_32FC(dims[3]), result_tensor[0]->data<float>().get());
        cv::resize(net_result_mat, net_result_mat, { img.dst_img.cols, img.dst_img.rows }, cv::INTER_LINEAR);
        cv::Mat img_id_mat;
        cv::Mat img_color_mat;
        ddrnet_map_plot(net_result_mat, img_id_mat, img_color_mat, dataset_name, img.dst_img);

        if (show) {
            cv::imshow("results", img_color_mat);
            cv::waitKey(0);
        }

        // save
        cv::imwrite(resRoot + "//" + name, img_id_mat);
        runtime.reset();
    }
    //icraft::dev::IcraftDeviceManager::close(device);

}

void rdn(const YAML::Node& network, const YAML::Node& param) {
    // 超分辨模型 RDN socket
    //-------------icraft config-----------------------//
    std::string url = network["url"].as<std::string>();
    std::string jsonPath = network["jsonPath"].as<std::string>();
    std::string rawPath = network["rawPath"].as<std::string>();
    std::string resRoot = network["resRoot"].as<std::string>();
    bool show = network["show"].as<bool>();
    checkDir(resRoot);

    // dataset relate
    std::string imgRoot = param["imgRoot"].as<std::string>();
    std::string imgList = param["imgList"].as<std::string>();

    //-------------SET ICORE-----------------------//
    auto device = icraft::dev::IcraftDeviceManager::open(url.data());
    auto network_ptr = std::make_shared<icraft::rt::RuntimeNetwork>(jsonPath.data(), rawPath.data());
    icraft::rt::Runtime runtime(network_ptr, device);
    NetInfo netinfo(network_ptr);
    runtime.speedMode();
    //runtime.setCalcTime(true); 
    runtime.apply();

    //-----支持network view模式，既任意算子段执行-----
    auto ops = network_ptr->getOps();
    auto runtime_view = runtime.view(ops.begin() + 1, ops.end());
    //------------- PICS -----------------------//
    int index = 0;
    auto namevector = toVector(imgList);
    int totalnum = namevector.size();
    for (auto name : namevector) {
        progress(index, totalnum);
        index++;
        //-------------PRE PROCESS-----------------------//
        std::string img_path = imgRoot + "\\" + name;
        PicPre img(img_path);
        img.Resize(netinfo.input_hw, PicPre::BOTH_SIDE);

        //-------------ICRAFT RUN-----------------------//
        auto img_tensor = netinfo.maketensor(device, img.dst_img);

        //auto result_tensor = runtime.forward({ img_tensor });
        auto result_tensor = runtime_view->forward({ img_tensor });

        //-------------POST PROCESS-----------------------//

        // 硬件排布转换为软件排布
        dataFormatH2S(result_tensor, device, netinfo.infos);

        auto dims = result_tensor[0]->dims();

        cv::Mat net_result_mat = cv::Mat(dims[1], dims[2], CV_32FC3, result_tensor[0]->data<float>().get());
        cv::cvtColor(net_result_mat, net_result_mat, cv::COLOR_RGB2BGR);
        if (show) {
            cv::imshow("results", net_result_mat);
            cv::waitKey(0);
        }

        // save
        //cv::imwrite(resRoot + name, net_result_mat * 255);
        runtime.reset();
    }
    //icraft::dev::IcraftDeviceManager::close(device);

}

void classify(const YAML::Node& network, const YAML::Node& param, const int inputFlag) {
    // 三通道图片分类模型 socket
    //-------------icraft config-----------------------//
    std::string url = network["url"].as<std::string>();
    std::string jsonPath = network["jsonPath"].as<std::string>();
    std::string rawPath = network["rawPath"].as<std::string>();
    std::string resRoot = network["resRoot"].as<std::string>();
    bool show = network["show"].as<bool>();
    checkDir(resRoot);
    // dataset relate
    std::string imgRoot = param["imgRoot"].as<std::string>();
    std::string imgList = param["imgList"].as<std::string>();
    std::string names_path = param["names"].as<std::string>();
    auto names = toVector(names_path);
    std::pair<int, int> resize_hw = param["resize_hw"].as<std::pair<int, int>>();
    std::pair<int, int> crop_hw = param["crop_hw"].as<std::pair<int, int>>();
    //-------------SET ICORE-----------------------//
    auto device = icraft::dev::IcraftDeviceManager::open(url.data());
    auto network_ptr = std::make_shared<icraft::rt::RuntimeNetwork>(jsonPath.data(), rawPath.data());
    icraft::rt::Runtime runtime(network_ptr, device);
    NetInfo netinfo(network_ptr);
    runtime.speedMode();
    //runtime.setCalcTime(true); 
    runtime.apply();

    //-----支持network view模式，既任意算子段执行-----
    auto ops = network_ptr->getOps();
    auto runtime_view = runtime.view(ops.begin()+1, ops.end());


    //------------- PICS -----------------------//
    int index = 0;
    auto namevector = toVector(imgList);
    int totalnum = namevector.size();
    for (auto name : namevector) {
        progress(index, totalnum);
        index++;
        //-------------PRE PROCESS-----------------------//
        std::string img_path = imgRoot + "\\" + name;
        PicPre img(img_path, inputFlag);
        img.Resize(resize_hw, PicPre::SHORT_SIDE).rCenterCrop(crop_hw);

        //-------------ICRAFT RUN-----------------------//
        auto img_tensor = netinfo.maketensor(device, img.dst_img);

        //auto result_tensor = runtime.forward({ img_tensor });
        auto result_tensor = runtime_view->forward({ img_tensor });
        //-------------POST PROCESS-----------------------//
        // 硬件排布转换为软件排布
        dataFormatH2S(result_tensor, device, netinfo.infos);
        auto results = result_tensor[0]->data<float>().get();
        auto res = softmax(results, 0, result_tensor[0]->dims()[1]);
        auto topk_res = topK(res, 5);
        classify_save(resRoot, name, topk_res);
        if (show) {
            classify_show(img.src_img, names_path, topk_res);
        }
        runtime_view->reset();
    }
    //icraft::dev::IcraftDeviceManager::close(device);

}

void yolo_seg_2stage(const YAML::Node& network, const YAML::Node& param, const int inputFlag) {

    //-------------icraft config-----------------------//
    std::string url = network["url"].as<std::string>();
    std::string jsonPath1 = network["jsonPath1"].as<std::string>();
    std::string rawPath1 = network["rawPath1"].as<std::string>();
    std::string jsonPath2 = network["jsonPath2"].as<std::string>();
    std::string rawPath2 = network["rawPath2"].as<std::string>();
    std::string resRoot = network["resRoot"].as<std::string>();
    bool show = network["show"].as<bool>();
    checkDir(resRoot);
    //--------------network config-----------------------//
    Yolo::extCfg cfg;
    // post relate
    cfg.CONF = param["conf"].as<float>();
    cfg.IOU = param["iou"].as<float>();
    cfg.NOC = param["number_of_class"].as<int>();
    cfg.NOH = param["number_of_head"].as<int>();
    cfg.ANCHORS = param["anchors"].as<std::vector<std::vector<std::vector<float>>>>();
    cfg.MULTILABLE = param["multilable"].as<bool>();

    // dataset relate
    std::string imgRoot = param["imgRoot"].as<std::string>();
    std::string imgList = param["imgList"].as<std::string>();
    std::string names_path = param["names"].as<std::string>();
    auto names = toVector(names_path);

    //-------------SET ICORE-----------------------//
    auto device = icraft::dev::IcraftDeviceManager::open(url.data());
    auto network_ptr1 = std::make_shared<icraft::rt::RuntimeNetwork>(jsonPath1.data(), rawPath1.data());
    auto network_ptr2 = std::make_shared<icraft::rt::RuntimeNetwork>(jsonPath2.data(), rawPath2.data());
    icraft::rt::Runtime runtime1(network_ptr1, device);
    icraft::rt::Runtime runtime2(network_ptr2, device);
    runtime1.apply();
    runtime2.apply();
    NetInfo netinfo(network_ptr1);

    //-----支持network view模式，既任意算子段执行-----
    auto ops = network_ptr1->getOps();
    auto runtime_view = runtime1.view(ops.begin()+1, ops.end());
    auto ops2 = network_ptr2->getOps();
    auto runtime_view2 = runtime2.view(ops2.begin() + 1, ops2.begin() + 2);
    auto runtime_view3 = runtime2.view(ops2.begin() + 2, ops2.end());

    //------------- PICS -----------------------//
    int index = 0;
    auto namevector = toVector(imgList);
    int totalnum = namevector.size();
    for (auto name : namevector) {
        progress(index, totalnum);
        index++;
        //-------------PRE PROCESS-----------------------//
        std::string img_path = imgRoot + "\\" + name;
        PicPre img(img_path, inputFlag);
        img.Resize(netinfo.input_hw, PicPre::LONG_SIDE).rPad();

        //-------------ICRAFT RUN-----------------------//
        auto img_tensor = netinfo.maketensor(device, img.dst_img);
        //auto result_tensor = runtime.forward({ img_tensor });
        auto result_tensor = runtime_view->forward({ img_tensor });

        //-------------POST PROCESS-----------------------//
        YoloV5_seg_2stage yolo;
        yolo.init(cfg, netinfo);
        //yolo->init_debug();
        yolo.filter(result_tensor);
        yolo.nms();
        yolo.coordTrans(img, runtime_view2, runtime_view3, device);
        if (show) {
            yolo.visualize(img.src_img, names);
        }
        yolo.saveRes(resRoot, name);
        runtime1.reset();
    }
    //icraft::dev::IcraftDeviceManager::close(device);
}
